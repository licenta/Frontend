require('dotenv').config();

var express = require('express')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var csrf = require('csurf')
var helmet = require('helmet')
var request = require('request')
var app = express()

app.disable('x-powered-by')
app.use(helmet())

var route_index = require('./routes/index');
var route_password = require('./routes/password');
var route_oauth2 = require('./routes/oauth2');

if (app.get('env') === 'development') {
    app.use(logger('dev'))
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser())

var api = createApiRouter()

// mount api before csrf is appended to the app stack
app.use('/oauth', api)

app.use(csrf({ cookie: true }))

app.use('/', route_index);
app.use('/password', route_password);
app.use('/oauth', route_oauth2);

function createApiRouter() {
    var router = new express.Router()
    router.post('/access_token', function (req, res) {
        request.post(process.env.TARGET_URL + '/oauth/access_token', {
            form: req.body
        }).pipe(res)
    });

    return router
}

app.use(express.static(__dirname + '/public'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500).send({
                message: err.message,
                error: err})
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500).send({
                message: err.message,
                error: err})
});


module.exports = app;
