import {globals, $http, setCookie, getCookie} from "~/util/util"
import module from "~/modules"

function load_session(response) {
    //clear the session
    sessionStorage.clear()
    //save the info in the session
    sessionStorage.setItem("userdata", response)
    response = JSON.parse(response)
    sessionStorage.setItem("token", response.access_token.access_token)
    sessionStorage.setItem("name", response.name)
    //TODO: Handle multiple roles
    sessionStorage.setItem("role", response.role[0].name)
}

function success_login(response) {
    var response_obj = JSON.parse(response);
    //save the cookie
    setCookie("response",response,response_obj.access_token.expires_in);

    load_session(response);
    //load the corresponding module based on user role
    var role = sessionStorage.getItem("role");
    module.load_role(()=> {
        switch (role){
            case "teacher":
                 window.location = "#/dash";
                 break;
            case "admin":
                 window.location = "#/subjects/admin";
                 break;
            case "student":
                 window.location = "#/subjects";
                 break;     
        }
       
    })
}

function failed_login(error) {
    console.log(error)
}

function login(username, password, success) {
    var local_success = success_login
    if(success !== null){
        local_success = success
    }
    $.ajax({
        "method": "get",
        "url": document.location,
        'processData': false,
        "success": function (response,status,request) {
            $http.post_raw(document.location.origin + "/oauth/access_token/password", {
                    username: username,
                    password: password
                }, {
                    headers: {
                        "csrf-token": request.getResponseHeader("csrf-token"),
                        "Content-Type": "application/json"
                    }
                })
                .then(local_success)
                .catch(failed_login)
        },
        "error": function (response) {
            console.log(response)
        }
    })
}

function logout() {
    //delete the cookie
    setCookie("response","a",0)
    sessionStorage.clear()
    window.location = "#/login"
}

export {login, logout, load_session}