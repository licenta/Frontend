<forget-form>
    <div class="authentication-container">
        <div class="authentication-box">
            <form onsubmit="{send}">
                <h2>Forgot password</h2>
                <input type="text" autocomplete="off" name="email" placeholder="Email" />
                <button type="submit">RESET</button>
            </form>
        </div>
    </div>
    <script>
        this.send = function(event){
          event.preventDefault()
          console.log(this.email.value)
        } 
    </script>
</forget-form>
