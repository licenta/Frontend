<login-form>
    <div class="authentication-container">
        <div class="authentication-box">
            <form onsubmit="{login}">
                <h2>Login into <span><i class="icon-lamp"></i> Luminous</span></h2>
                <input type="text" autocomplete="on" name="username" placeholder="Username" />
                <input type="password" name="password" placeholder="Password" />
                <button type="submit">LOGIN</button>
            </form>
            <a href="#/forget" class="forget-link">Forget your password</a>
        </div>
    </div>
    <script>
        import {login} from "./authService"

        this.login = function (event) {
            event.preventDefault()
            login(this.username.value, this.password.value, null)
        }
    </script>
</login-form>