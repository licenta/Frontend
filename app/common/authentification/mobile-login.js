import {login, load_session} from "./authService"
import {setCookie} from "~/util/util"

var vm = this

this.login = function (event) {
    event.preventDefault()
    login(this.username.value, this.password.value, function (response) {
        var response_obj = JSON.parse(response)

        load_session(response)

        var param = vm.opts.param
        var querystring = "?client_id=" + param.client_id + "&redirect_uri=" + param.redirect_uri
            + "&response_type=" + param.response_type + "&scope=" + param.scope + "&state=" + param.state
        window.location = "#/scopes" + querystring
    })
}
