<mobile-login class="container-fluid">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <ui-view>
                <h1 class="text-center login-title">UBB_Licenta</h1>
                <div class="account-wall">
                    <form onsubmit="{login}" class="form-signin">
                        Email:
                        <br><input type="text" class="form-control" name="username" required autofocus/>
                        <br>Password:
                        <br><input type="password" class="form-control" name="password" required/>
                        <br>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Login in</button>
                    </form>
                </div>
                <a href="#/forget" class="text-center new-account">Forget your password</a>
            </ui-view>
        </div>
    </div>
    <script>
        import {login,load_session} from "./authService"
        import {setCookie} from "~/util/util"

        var vm = this

        this.login = function (event) {
            event.preventDefault()
            login(this.username.value, this.password.value,function(response){
                var response_obj = JSON.parse(response)


                //setCookie("response",response,response_obj.access_token.expires_in)

                load_session(response)

                var param = vm.opts.param
                var querystring = "?client_id=" + param.client_id + "&redirect_uri=" + param.redirect_uri
                        + "&response_type=" + param.response_type + "&scope=" + param.scope + "&state=" + param.state
                window.location = "#/scopes" + querystring
            })
        }
    </script>
</mobile-login>
