import "./login-form.tag"
import "./forget-form.tag"
import "./mobile-login.tag"
import {stateRouter} from "~/router"
import {getCookie, $http} from "~/util/util"
import {load_session} from "./authService"
import "./user-scopes.tag"

const states = [{
    name: 'login',
    route: '/login',
    template: 'login-form',
    resolve: (data, form, call)=> {
        var response = getCookie("response")
        //if we are logged in the user can't see the login page
        if (response !== "") {
            load_session(response)
            window.location = "#/subjects"
        } else {
            call(null, null)
        }
    }
}, {
    name: 'login_mobile',
    route: '/oauth/authorize',
    template: 'mobile-login',
    resolve: (data, form, call)=> {
        var response = getCookie("response")
        //if we are logged in the user can't see the login page
        var querystring = "?client_id=" + form.client_id + "&redirect_uri=" + form.redirect_uri + "&response_type=" + form.response_type + "&scope=" + form.scope + "&state=" + form.state
        
        if (response !== "") {
            load_session(response)
            window.location = "#/scopes" + querystring
        } else {
            call(null, null)
        }
    },
    activate: (context)=> {
        var tag = context.domApi
        tag.opts.param = context.parameters
    }
}, {
    name: 'user_scopes',
    route: '/scopes',
    template: 'user-scopes',
    activate: (context)=> {
        var tag = context.domApi
        var form = context.parameters
        var querystring = "?client_id=" + form.client_id + "&redirect_uri=" + form.redirect_uri + "&response_type=" + form.response_type + "&scope=" + form.scope + "&state=" + form.state
        console.log(form)
        $http.get(window.location.origin + "/oauth/authorize" + querystring)
            .then(function (response_param){
                var response = JSON.parse(response_param)
                if(response.redirect_uri){
                    console.log("You all ready accepted the rights")
                    window.location.replace(response.redirect_uri)
                }
                tag.opts.request = response
                tag.update()
            })
    }
}, {
    name: 'forget',
    route: '/forget',
    template: 'forget-form',
    resolve: (data, form, call)=> {
        var response = getCookie("response")
        //if we are logged in the user can't see the login page
        if (response !== "") {
            load_session(response)
            window.location = "#/subjects"
        } else {
            call(null, null)
        }
    }
}
]

export default states
