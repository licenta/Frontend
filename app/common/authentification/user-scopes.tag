<user-scopes class="container-fluid">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4"  show="{this.opts.request.params.scopes}">
            <h3>The app will have the following rights:</h3>
            <div class="text-center login-title" each="{scope in this.opts.request.params.scopes}" >
                <!--TODO: Mihai nu mi-ai dat bine scope in Array nu ii consistent ar trebui sa fie name:scope -->
                {scope}
            </div>
            <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit" onclick="{accept}">Accept</button>
            <button class="btn btn-lg btn-primary btn-block" type="button" onclick="{decline}">Decline</button>
        </div>

        <div class="col-sm-6 col-md-4 col-md-offset-4"  show="{this.opts.request.redirect_uri}">
            <h3>You all ready accepted the scopes:</h3>
        </div>
    </div>
    <script>
        import {$http} from "~/util/util"

        var vm = this

        this.accept = function () {
            var config = this.opts.request.params

            $.ajax({
                "method": "get",
                "url": document.location,
                'processData': false,
                "success": function (response, status, request) {
                    $http.post_raw(document.location.origin + "/oauth/authorize", {
                                "client_id": config.client_id,
                                "redirect_uri": config.redirect_uri,
                                "state": config.state,
                                "scope": config.scope,
                                "response_type": config.response_type
                            }, {
                                headers: {
                                    "Content-Type": "application/json",
                                    "csrf-token": request.getResponseHeader("csrf-token"),
                                    "Authorization": "Bearer " + sessionStorage.getItem("token")
                                }
                            })
                            .then(function (response) {
                                var url = JSON.parse(response).redirect_uri
                                window.location.replace(url);
                            })
                            .catch(function (response) {
                                console.log(response)
                            })
                },
                "error": function (response) {
                    console.log(response)
                }
            })
        }
        this.decline = function () {
            console.log("Decline")
        }
    </script>
</user-scopes>