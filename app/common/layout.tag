<layout>
	<!--Navbar-->
	<div class="navbar-container">
		  <nav class="navbar">
		    <div class="navbar-header">
                <a class="navbar-brand" href="#/subjects/admin"><i class="icon-lamp"></i> Luminous</a>
			</div>
	
			<div class="collapse navbar-collapse js-navbar-collapse">
		        <ul class="nav navbar-nav navbar-right">
			        <li>
			          <div>
			          	<div id="user-data">
			          		<img src="http://www.keenthemes.com/preview/conquer/assets/plugins/jcrop/demos/demo_files/image1.jpg" alt="" class="img-circle">
			          	 	<span>User Name</span>
			          	</div>
			          	 <a href="#/login" aria-label="Left Align">
						  <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
						</a>
						 <a href="#"  aria-label="Left Align">
						  <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
			          </div>
			        </li>
		      	</ul>
			</div><!-- /.nav-collapse -->
  		</nav>
	</div>

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <nav id="spy">
            <ul class="sidebar-nav nav">
                <li>
                    <a href="" onclick={go}>
                    	<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    	<span class="fa fa-home solo">Courses</span>
                	</a>
                </li>
                <li>
                	
                    <a href="#">
                    	<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        <span class="fa fa-anchor solo">Home</span>
                    </a>
                </li>
                <li>
                	
                    <a href="#" data-scroll>
                    	<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        <span class="fa fa-anchor solo">Home</span>
                    </a>
                </li>
                <li>
                    <a href="#" data-scroll>
                    	<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        <span class="fa fa-anchor solo">Home</span>
                    </a>
                </li>
                <li>
                    <a href="#" data-scroll>
                    	<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        <span class="fa fa-anchor solo">Home</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    
    <div id="main-content"></div>

    <script>
    import "../course/course-layout.tag"
    this.go = function(event){
        $(event.target).focus();
        riot.mount("#main-content",'course-layout');
    }
    
</script>
</layout>

