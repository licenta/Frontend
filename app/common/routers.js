import states_auth from "./authentification/routes"
import states_subject from "./subject/routes"
import {reducerRegistry} from "~/reduxStore"

import subject from "./subject/reducer"

var states = [
    ...states_auth,
    ...states_subject
]

reducerRegistry.register([
    {"subject_c":subject}
])

export default states