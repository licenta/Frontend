import {globals, $http, BASE_API} from "~/util/util"

const load_subjects = (url)=> {
    return (dispatch, getState)=> {
        $http.get(globals.BASE_API + url )
            .then(function (response) {
                dispatch(subjects_loaded(JSON.parse(response).subject))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const subjects_loaded = (subjects)=> {
    return {
        type: "SUBJECTS_LOADED",
        data: subjects
    }
}

export {load_subjects}