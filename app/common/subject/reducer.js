export default (state={title:"Default title"},action={})=>{
    switch(action.type){
        case 'SUBJECTS_LOADED':
            return Object.assign({},state,{subjects:action.data})
        default:
            return state
    }
}