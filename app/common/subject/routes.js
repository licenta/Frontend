import "./subject-layout.tag"
import "./filter-subject.tag"
import "./submenu-subject.tag"

import {reduxStore} from "~/reduxStore"

const states = [{
    name: 'layout.subject',
    route: '/subjects',
    template: 'subject-layout',
    activate:function(context) {
        var tag = context.domApi
        tag.opts.store = reduxStore
    }
}]

export default states