<subject-layout>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <h1>Subjects</h1>
            </div>
            <div class="col-xs-12 col-sm-8">
                <submenu-subject />
            </div>
        </div>
        <div class="line"></div>
        <div class="courses-container">
            <ui-view/>
        </div>
    </div>
    <script>
        
    </script>
</subject-layout>