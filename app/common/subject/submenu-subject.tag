<submenu-subject>
    <!-- <div id="course-tab" class="btn-group" data-toggle="buttons-checkbox">
        <a onclick={changeSelectedActivity} class="btn btn-large btn-info activity active">all</a>
        <a  onclick={changeSelectedActivity} each={activity_types} class="btn btn-large btn-info activity">{name}</a>
    </div> -->
    <script>
        import {globals, $http} from "~/util/util"
        this.activity_types =[];
        
         this.on("mount", function(){
              localStorage.setItem('selected_actrivity', "");
             var self = this;
             
             $http.get(globals.BASE_API + "/activity-type?client_id=" + globals.client_id)
            .then(function (response) {
                var data = JSON.parse(response);
                self.activity_types = data.activity_type;
                self.update();
            }).catch((error)=> {
            console.log(error);
            })
        }) 
        
        this.on("updated", function(){
             $('.activity').click(function(event){
                event.stopPropagation();
                $('.activity').removeClass('active');
                $(this).addClass('active');
               
            })
        })
        
        this.changeSelectedActivity = function(event){
            if(event.item == undefined){
                localStorage.setItem('selected_actrivity', "");
        
            }else{
                localStorage.setItem('selected_actrivity', event.item.id);
            }
        }
             
       
        
    </script>
</submenu-subject>