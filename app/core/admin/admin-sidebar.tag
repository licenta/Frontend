<admin-sidebar>
    <li>
        <a class="menu-item" href="#/faculty">
            <span class="icon-bank" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Faculty</span>
        </a>
    </li>
    <li>
        <a class="menu-item" href="#/sections">
            <span class="icon-book" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Section</span>
        </a></li>
    <li>
        <a class="menu-item" href="#/departments">
            <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Departments</span>
        </a></li>
    <li>
        <a class="menu-item" href="#/curriculum">
            <span class="icon-books" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Curriculum</span>
        </a></li>
    <li>
        <a class="menu-item" href="#/subjects/admin">
            <span class="icon-pencil2" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Subjects</span>
        </a></li>
    <li>
        <a class="menu-item" href="#/users">
            <span class="icon-users" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">User management</span>
        </a></li>
    <li>
        <a class="menu-item" href="#/activity-types">
            <span class="icon-file-empty" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Activity Types</span>
        </a></li>
    <li>
        <a class="menu-item" href="#/activity-types">
            <span class="icon-file-text2" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Evaluation Types</span>
        </a>
    </li>
    <script>
        this.on("update", function(){
            var items = $('.menu-item');
            $.each(items, function(index, value){
                if($(value).attr("href") == window.location.hash){
                    $(value).addClass("selected");
                }
            }) 
            
            $('.menu-item').click(function(){
                $(".menu-item").removeClass("selected");
                $(this).addClass("selected");
            })
        })
        
     
    </script>
</admin-sidebar>