<add-department>
    <div class="modal fade" id="addDepartmentModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Add a new Department</h4>
                </div>
                <div class="modal-body row">
                    <form id="delDep">
                        <div class="col-md-12 col-xs-12">
                            <label>Department Name:</label>
                            <p><input type="text" name="name" id="name" placeholder="Department Name" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-xs-12 col-md-12 select-container">
                            <label>Faculty:</label>
                            <select class="selectpicker show-tick btn-labeled" name="faculty" data-live-search="true" style="display: none;" data-style="btn-success">
                                <option each="{faculties}" value="{id}" >
                                    {name}
                                </option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button"
                              onclick="{submit_form}"><span class="glyphicon glyphicon-check"></span> Add Department
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <script>
        import {globals, $http} from "~/util/util"
        var vm = this;
        this.on('updated', function() {
            $('.selectpicker').selectpicker('refresh');
        })
         
         vm.faculties = [];
         
         this.on("mount", function(){
            $http.get(globals.BASE_API + "/faculty?client_id=" + globals.client_id)
                .then(function (response) {
                    vm.faculties = JSON.parse(response).faculty.data;
                }).catch((error)=> {
                console.log(error)
            })
            
            $("#delDep").validate({
                rules:{
                    name: {
                        required: true
                    }
                },
                messages:{
                    name:"Please specify a department name"
                },
                 submitHandler: function() {
                   var newDep={
                        "name":vm.name.value,
                        "faculty_id":vm.faculty.value
                    }
                    
                    vm.parent.opts.add_department(newDep);
                    vm.name.value = null;
                    $("#addDepartmentModal").modal('hide');
                }
            })
         })
         
         this.submit_form = function(){
           $("#delDep").submit();
         }
        
    </script>
</add-department>