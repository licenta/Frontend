<department-layout>
    <div class="container-fluid">
        <h1>Departments</h1>
        <div class="line"></div>
        <filter-department data="{this.state.faculty}" select_faculty="{select_faculty}" add_department="{add_department}"/>
        <div class="departments-container row">
                <department-list data="{this.state.departments.data}" delete_department="{delete_department}">
                </department-list>
        </div>
        <nav>
            <ul class="pager">
                <li onclick="{prev_page}" class="previous disabled"><a ><span aria-hidden="true">&larr;</span> Previous</a></li>
                <li onclick="{next_page}" class="next"><a >Next <span aria-hidden="true">&rarr;</span></a></li>
            </ul>
        </nav>
    </div>
    <script>
        import * as actions from "./departmentActions"
        import {load_departments} from "./departmentActions"
        import {globals} from "~/util/util"
        
        var vm = this;
        var update = vm.update
        var state;
        var ok = 0
        
        vm.current_faculty = null;
        
        vm.add_department = function (department) {
            vm.opts.store.dispatch(actions.add_department(department))
        }
         
        vm.delete_department = function (department_id) {
            vm.opts.store.dispatch(actions.delete_department(department_id))
        }
        
        vm.select_faculty = function (current_faculty) {
            vm.state = [];
            vm.current_faculty = current_faculty;
            vm.opts.store.dispatch(load_departments(globals.BASE_API + "/faculty/" + vm.current_faculty + "/department?with=language&client_id=" + globals.client_id));
            vm.state = vm.opts.store.getState().departments;
            vm.update();
        }
        
        vm.on("update", function () {
            console.log("update")
            if (vm.opts.store && ok == 0) {
                vm.opts.store.subscribe(function () {
                    update()
                }).bind(vm)
            }
            if (vm.opts.store) {
                ok = 1
                vm.state = vm.opts.store.getState().departments
            }
            
            $(".pager").hide();
            if(vm.state && vm.state.departments && vm.state.departments.data){
                if(vm.state.departments.data.length == 0) {
                    $(".pager").hide();
                    return;
                }
                $(".pager").show();
                if(vm.state.departments.next_page_url != null){
                    $(".next").removeClass("disabled");
                }else{
                    $(".next").addClass("disabled");
                }
            
                if(vm.state.departments.prev_page_url != null){
                    $(".previous").removeClass("disabled");
                }else{
                    $(".previous").addClass("disabled");
                }
                
            }
            
            vm.next_page = function(event){
                if(vm.state){
                    if(vm.state.departments.next_page_url != null){
                        $(event.target).removeClass("disabled")
                        vm.opts.store.dispatch(load_departments(vm.state.departments.next_page_url + "&client_id="+globals.client_id ));
                        vm.state = vm.opts.store.getState().departments; 
                    }else{
                        $(event.target).addClass("disabled")
                    }
                }
            }
        
            vm.prev_page = function(event){
                if(vm.state){
                    if(vm.state.departments.prev_page_url != null){
                        $(event.target).removeClass("disabled")
                        vm.opts.store.dispatch(load_departments(vm.state.departments.prev_page_url + "&client_id="+globals.client_id ));
                        vm.state = vm.opts.store.getState().departments;
                    }else{
                        $(event.target).addClass("disabled")
                    }
                }
            }
        });
        
        this.on("unmount", function () {
            this.state.departments = null;
        })
    </script>
</department-layout>