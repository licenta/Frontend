<department-submenu>
    <div class="submenu">
        <a class="btn btn-primary addDep" value="right" type="button" data-toggle="modal" href="#addDepartmentModal">
            <i class="fa fa-fw fa-cog"></i>+
        </a>
        <add-department add-department="{add_department}"/>
    </div>
</department-submenu>