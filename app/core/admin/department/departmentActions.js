import {globals, $http} from "~/util/util"

const load_departments = (url)=> {
    return (dispatch, getState)=> {

        $http.get(url)
            .then(function (response) {
                dispatch(departments_loaded(JSON.parse(response).department));
            }).catch((error)=> {
            console.log(error);
        })
    }
}
const add_department = (data)=> {
    return (dispatch, getState)=> {
        $http.post(globals.BASE_API + "/department",data)
            .then(function (response) {
                dispatch(department_added(JSON.parse(response).department));
            }).catch((error)=> {
            var message = JSON.parse(error.responseText).error.name
            console.log(error);
        })
    }
}
const delete_department = (data)=> {
    return (dispatch, getState)=> {
        $http.delete(globals.BASE_API + "/department/" + data)
            .then(function (response) {
                dispatch(department_deleted({id:data}));
            }).catch((error)=> {
            console.log(error);
        })
    }
}

const department_deleted = (department)=> {
    return {
        type: "DEPARTMENT_DELETED",
        data: department
    }
}
const department_added = (department)=> {
    return {
        type: "DEPARTMENT_ADDED",
        data: department
    }
}
const departments_loaded = (departments)=> {
    return {
        type: "DEPARTMENTS_LOADED",
        data: departments
    }
}
const load_faculties = ()=> {
    return (dispatch, getState)=> {
       $http.get(globals.BASE_API + "/faculty?client_id=" + globals.client_id)
            .then(function (response) {
               
                dispatch(faculties_loaded(JSON.parse(response).faculty.data))
            }).catch((error)=> {
            console.log(error)
        })
    }
}
const faculties_loaded = (faculty)=> {
    return {
        type: "FACULTIES_LOADED",
        data: faculty
    }
}

export {load_departments, add_department, delete_department, load_faculties}