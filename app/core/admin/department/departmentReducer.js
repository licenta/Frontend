const departmentReducer = (state={message:""},action={})=>{
    switch(action.type){
        case 'DEPARTMENTS_LOADED':
            return Object.assign({},state,{departments:action.data});
        case 'DEPARTMENT_ADDED':
            if(state.departments.next_page_url === null){
                 var obj = state.departments.data.concat(action.data);
                 state.departments.data = obj;
                 return Object.assign({},state,{departments:state.departments})
            }else{
                 return Object.assign({},state,{departments:state.departments})
            }
        case 'DEPARTMENT_DELETED':
            var departmentIndex = state.departments.data.findIndex(function(department){
                return department.id == action.data.id
            })
            
            var obj1 = state.departments.data.slice(0,departmentIndex);
            var obj2 = state.departments.data.slice(departmentIndex + 1);
            state.departments.data = obj1.concat(obj2);
            return Object.assign({},state,{departments: state.departments})
        case 'FACULTIES_LOADED':
            return Object.assign({},state,{faculty:action.data});
        default:
            return state
    }
}

export default departmentReducer