<filter-department>
    <div class="auto-select row">
        <div class="col-xs-12 col-sm-6 col-md-6 select-container department-select">
            <label>Select Faculty:</label>
            <select class="selectpicker show-tick btn-labeled" name="faculty" data-live-search="true" style="display: none;" onchange={select_faculty} data-style="btn-success">
                <option>Nothing Selected</option>
                <option each="{this.opts.data}" value="{id}">
                    {name}
                </option>
            </select>
            <div class="submenu">
                <a class="btn btn-success addDep" value="right" type="button" data-toggle="modal" href="#addDepartmentModal">+</a>
                <add-department add-department="{add_department}"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
        </div>
    </div>
    <script>
        var vm = this;
        
        this.select_faculty = function(){
            vm.opts.select_faculty(this.faculty.value);    
        }
    </script>
</filter-department>