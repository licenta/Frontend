import "./department-layout.tag"
import "./department-list.tag"
import "./department-submenu.tag"
import "./add-department.tag"
import "./delete-modal-department.tag"
import "./filter-department.tag"

import {load_faculties} from "./departmentActions"

import {reduxStore} from "~/reduxStore"

import {globals} from "~/util/util"

console.log("activate");
const states = [{
    name: 'layout.department',
    route: '/departments',
    template: 'department-layout',
    activate: (context)=> {
        console.log("activate");
        var tag = context.domApi;
        tag.opts.store = reduxStore;
        tag.opts.store.dispatch(load_faculties());
        tag.update();
    }
}]

export default states