<delete-modal-faculty>
    <div id="{this.opts.data.id}" class="modal fade in deleteFacModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Delete Faculty</h4>
                </div>
                <div class="modal-body">
                    <h5>Are you sure you want to delete faculty {this.opts.data.name}?</h5>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" value="{this.opts.data.id}" type="button" data-dismiss="modal"
                              onclick="{delete_faculty}"><span class="glyphicon glyphicon-check"></span> Yes
                        </button>
                    </div>
                </div>
 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dalog -->
    </div><!-- /.modal -->
    <script>
        var vm = this

        this.delete_faculty = function (event) {
            vm.parent.opts.delete_faculty(event.target.value)
        }
    </script>
</delete-modal-faculty>