<faculty-layout>
    <div class="container-fluid">
        <h1>Facultys</h1>
        <div class="line"></div>
        <div class="faculty-container row">
            <div class="submenu">
                <form-faculty
                    add_faculty="{add_faculty}"
                    message="{this.state.message}"
                />
            </div>
            <faculty-list data="{this.state.facultys.data}"
                delete_faculty="{delete_faculty}"
            />
            <nav>
                <ul class="pager">
                    <li onclick="{prev_page}" class="previous disabled"><a ><span aria-hidden="true">&larr;</span> Previous</a></li>
                    <li onclick="{next_page}" class="next"><a >Next <span aria-hidden="true">&rarr;</span></a></li>
                </ul>
            </nav>
        </div>
        <script>
            import * as actions from "./facultyActions"
            import {load_facultys} from "./facultyActions"
            
            var vm = this
            var update = vm.update
            var ok = 0
            vm.add_faculty = function (faculty) {
                vm.opts.store.dispatch(actions.add_faculty(faculty))
            }
            vm.delete_faculty = function (faculty_id) {
                vm.opts.store.dispatch(actions.delete_faculty(faculty_id))
            }
            vm.on("update", function () {
                if (vm.opts.store && ok == 0) {
                    vm.opts.store.subscribe(function () {
                        update()
                    })
                }
                if (vm.opts.store && vm.opts.store.getState().facultys) {
                    ok = 1
                    vm.state = vm.opts.store.getState().facultys
                    if(!vm.state.facultys.data){
                        vm.state = null;
                    }
                }
                $(".pager").hide();
                if(vm.state){
                    if(vm.state.facultys){
                        if(vm.state.facultys.data.length == 0) {
                            $(".pager").hide();
                            return;
                        }
                        $(".pager").show();
                        if(vm.state.facultys.next_page_url != null){
                          $(".next").removeClass("disabled");
                        }else{
                          $(".next").addClass("disabled");
                        }
                    
                        if(vm.state.facultys.prev_page_url != null){
                            $(".previous").removeClass("disabled");
                        }else{
                            $(".previous").addClass("disabled");
                        }
                    }
                }
                
                vm.next_page = function(event){
                    if(vm.state){
                        if(vm.state.facultys.next_page_url != null){
                            $(event.target).removeClass("disabled")
                            vm.opts.store.dispatch(load_facultys(vm.state.facultys.next_page_url + "&client_id="+globals.client_id ));
                            vm.state = vm.opts.store.getState().faculty; 
                        }else{
                            $(event.target).addClass("disabled")
                        }
                    }
                }
        
                vm.prev_page = function(event){
                    if(vm.state){
                        if(vm.state.facultys.prev_page_url != null){
                            $(event.target).removeClass("disabled")
                            vm.opts.store.dispatch(load_facultys(vm.state.facultys.prev_page_url + "&client_id="+globals.client_id ));
                            vm.state = vm.opts.store.getState().faculty;
                        }else{
                            $(event.target).addClass("disabled")
                        }
                    }
                }
            });
        </script>
</faculty-layout>