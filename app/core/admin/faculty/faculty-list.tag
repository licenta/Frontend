<faculty-list>
  <div class="section">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-striped">
                    <tbody>
                        <tr each={this.opts.data} data="{ this }">
                            <td>
                               <h4>
                                    <b><p> <span class="icon-school"></span></p> </b>
                               </h4>
                            </td>
                            <td>
                                <h4>
                                    <b>{this.name} {this.language.name}</b>
                                </h4>
                            </td>
                            <td>
                                </h4>
                            </td>
                            <td>
                                <a class="btn btn-primary" id="deleteFaculty" href="#{this.id}" data-toggle="modal"
                                        value="{id}" type="button" >
                                    <i class="fa fa-fw fa-cog"></i>x
                                </a>
                                <delete-modal-faculty data="{this}" delete_faculty="{delete_faculty}" />
                            </td>
                        </tr>                
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</faculty-list>