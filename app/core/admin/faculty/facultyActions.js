import {globals, $http} from "~/util/util"

const load_facultys = (url)=> {
    return (dispatch, getState)=> {

        $http.get(url)
            .then(function (response) {
                dispatch(facultys_loaded(JSON.parse(response).faculty));
            }).catch((error)=> {
            console.log(error);
        })
    }
}
const add_faculty = (data)=> {
    return (dispatch, getState)=> {
        $http.post(globals.BASE_API + "/faculty",{name:data})
            .then(function (response) {
                dispatch(faculty_added(JSON.parse(response).faculty));
            }).catch((error)=> {
            var message = JSON.parse(error.responseText).error.name
            dispatch(faculty_error(message))
            console.log(error);
        })
    }
}
const delete_faculty = (data)=> {
    return (dispatch, getState)=> {
        $http.delete(globals.BASE_API + "/faculty/" + data)
            .then(function (response) {
                dispatch(faculty_deleted({id:data}));
            }).catch((error)=> {
            console.log(error);
        })
    }
}

const faculty_deleted = (faculty)=> {
    return {
        type: "FACULTY_DELETED",
        data: faculty
    }
}
const faculty_added = (faculty)=> {
    return {
        type: "FACULTY_ADDED",
        data: faculty
    }
}
const facultys_loaded = (facultys)=> {
    return {
        type: "FACULTYS_LOADED",
        data: facultys
    }
}
const faculty_error = (message)=> {
    return {
        type: "SHOW_ERROR",
        data: message
    }
}

export {load_facultys,add_faculty,delete_faculty}