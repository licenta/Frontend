const facultyReducer = (state = {message: "", current_page: 1,
    facultys:[],isLoading:false}, action = {})=> {
    switch (action.type) {
        case 'FACULTYS_LOADED':
            return Object.assign({},state,{facultys:action.data});
        case 'FACULTY_LOADING':
            return Object.assign({}, state, {isLoading:action.data})
        case 'FACULTY_LOAD_MORE':
            return Object.assign({}, state, {current_page:action.data})
        case 'FACULTY_ADDED':
            return Object.assign({}, state, {facultys: state.facultys.concat(action.data)})
        case 'FACULTY_DELETED':
            var facultyIndex = state.facultys.findIndex(function (faculty) {
                return faculty.id == action.data.id
            })
            return Object.assign({}, state, {
                facultys: [
                    ...state.facultys.slice(0, facultyIndex),
                    ...state.facultys.slice(facultyIndex + 1)
                ]
            })
        case 'SHOW_ERROR':
            return Object.assign({}, state, {message: action.data})
        default:
            return state
    }
}

export default facultyReducer