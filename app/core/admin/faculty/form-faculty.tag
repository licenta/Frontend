<form-faculty>
    <button type="button" class="btn btn-info addFaculty"
            data-toggle="modal" data-target="#addFacultyModal">
        +
    </button>
    
    <div class="modal fade" id="addFacultyModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                        <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                        <h4 class="modal-title" style="float:left">Add a new Faculty</h4>
                    </div>
                    <div class="modal-body row">
                        <div class="col-md-12 col-xs-12">
                            <label>Faculty Name:</label>
                            <p><input type="text" name="newName" id="newName" placeholder="Faculty Name" style="border-radius:4px;width:100%"></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group">
                            <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                            <button  data-dismiss="modal" class="btn btn-primary btnYes" type="submit" onclick={handleSubmit}
                                ><span class="glyphicon glyphicon-check"></span> Add Faculty
                            </button>
                        </div>
                    </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <script>
        var vm = this

        this.handleSubmit = function () {
            vm.opts.add_faculty(this.newName.value)
        }
    </script>
</form-faculty>