import "./faculty-layout.tag"
import "./faculty-list.tag"
import "./form-faculty.tag"
import "./delete-modal-faculty.tag"

import {globals} from "~/util/util"

import {load_facultys} from "./facultyActions"

import {reduxStore} from "~/reduxStore"


const states = [{
    name: 'layout.faculty',
    route: '/faculty',
    template: 'faculty-layout',
    activate: (context)=> {
        var tag = context.domApi;
        tag.opts.store = reduxStore;
        tag.opts.store.dispatch(load_facultys(globals.BASE_API + "/faculty?client_id="+globals.client_id));
        tag.update();
    }
}]

export default states