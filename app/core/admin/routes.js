import {load_states,stateRouter} from "~/router"
import states_subject from "./subject/routes"
import {reducerRegistry} from "~/reduxStore"
import states_users from "./user/routes"
import states_sections from "./section/routes"
import states_departments from "./department/routes"
import states_facultys from "./faculty/routes"


import "./admin-sidebar.tag"

//here we import the reducers
import subject from "./subject/subjectReducer"
import facultys from "./faculty/facultyReducer"
import departments from "./department/departmentReducer"
import user from "./user/userReducer"
import sections from "./section/sectionReducer"

init()
function init() {
    
    //here we load the our reducer at runtime
    reducerRegistry.register([
        {"subject":subject},
        {"facultys":facultys},
        {"departments":departments},
        {"user":user},
        {"sections":sections}
    ])

    //here you add the routes for the admin
    console.log("lala")
    load_states([
        ...states_subject,
        ...states_users,
        ...states_sections,
        ...states_departments,
        ...states_facultys
    ])
    //after we load the states we evalute the current layout
    stateRouter.evaluateCurrentRoute("layout")
}
