<add-section>
    <div class="modal fade" id="addSectionModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Add a new Section</h4>
                </div>
                <div class="modal-body row">
                    <form id="addSec">
                        <div class="col-md-12 col-xs-12">
                            <label>Section Name:</label>
                            <p><input type="text" name="name" id="name" placeholder="Section Name" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Faculty:</label>
                            <select class="selectpicker show-tick btn-labeled" name="faculty" data-live-search="true" style="display: none;" data-style="btn-success"
                                >
                                <option each="{this.opts.data}" value="{id}" >
                                    {name}
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Language:</label>
                            <select class="selectpicker show-tick" name="language" data-live-search="true"  style="display: none;"  data-style="btn-success">
                                <option each="{this.opts.languages}" value="{id}" >
                                    {name}
                                </option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button"
                              onclick="{submit_form}" ><span class="glyphicon glyphicon-check"></span> Add Section
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <script>
        import {globals, $http} from "~/util/util"
        var vm = this;
        this.on('updated', function() {
               $('.selectpicker').selectpicker('refresh');
         })
        
        this.on('mount', function(){
            $("#addSec").validate({
                rules:{
                    name:{
                        required: true
                    }
                },
                messages:{
                    name:"Please specify a section name."
                },
                submitHandler: function() {
                    var newSec={
                        "name":vm.name.value,
                        "faculty_id":vm.faculty.value,
                        "language_id":vm.language.value
                    }
                    
                    vm.parent.opts.add_section(newSec);
                    vm.name.value = null;
                    $("#addSectionModal").modal('hide');
                }
            })
        });
         
         this.submit_form = function(){
            $("#addSec").submit();
         }
        
    </script>
</add-section>