<delete-modal>
    <div id="{this.opts.data.id}" class="modal fade in deleteSectionModal">
        <div class="modal-dialog">
            <div class="modal-content">
 
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Delete Section</h4>
                </div>
                <div class="modal-body">
                    <h5>Are you sure you want to delete section {this.opts.data.name}?</h5>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" id="buttonYes"  value="{id}" type="button" data-dismiss="modal"
                              onclick="{delete_section}"><span class="glyphicon glyphicon-check"></span> Yes
                        </button>
                    </div>
                </div>
 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dalog -->
    </div><!-- /.modal -->
    <script>
        import {globals, $http} from "~/util/util"
        var vm = this

        this.delete_section = function (event) {
            vm.parent.opts.delete_section(this.opts.data.id);
        }
    </script>
</delete-modal>