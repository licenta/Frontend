<filter-sections>
    <div class="auto-select row">
        <div class="col-xs-12 col-sm-6 col-md-6 select-container section-select">
            <label>Select Faculty:</label>
            <select class="selectpicker show-tick btn-labeled" name="faculty" data-live-search="true" style="display: none;" onchange={select_faculty} data-style="btn-success">
                <option>Nothing Selected</option>
                <option each="{this.opts.data}" value="{id}">
                    {name}
                </option>
            </select>
            <div class="submenu">
                <a class="btn btn-primary addSection" data-toggle="modal" href="#addSectionModal" type="button">
                    <i class="fa fa-fw fa-cog"></i>+
                </a>
                <add-section add-section={"add_section"} data={this.opts.data} languages={this.opts.languages}/>
            </div>
        </div>
    </div>
    <script>
        import {globals, $http} from "~/util/util"
        var vm = this;
        vm.faculties = null;
        
        this.select_faculty = function(){
            vm.opts.select_faculty(this.faculty.value);    
        }
    </script>
</filter-sections>