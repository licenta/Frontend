import "./section-layout.tag"
import "./section-list.tag"
import "./section-submenu.tag"
import "./delete-modal.tag"
import "./add-section.tag"
import "./filter-sections.tag"

import {load_faculties, load_languages} from "./sectionActions"

import {reduxStore} from "~/reduxStore"

import {globals} from "~/util/util"

const states = [{
    name: 'layout.section',
    route: '/sections',
    template: 'section-layout',
    activate: (context)=> {
        var tag = context.domApi;
        tag.opts.store = reduxStore;
        tag.opts.store.dispatch(load_faculties());
        tag.opts.store.dispatch(load_languages());
        tag.update();
    }
}]

export default states