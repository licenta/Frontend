<section-layout>
    <div class="container-fluid">
        <h1>Sections</h1>
        <div class="line"></div>
        <filter-sections data={this.state.faculty} select_faculty={select_faculty} add_section="{add_section}" 
               languages={this.state.languages}/>
        <div class="sections-container row">
             <section-list data="{this.state.sections.data}" delete_section="{delete_section}">
             </section-list>
        
            <nav>
                <ul class="pager">
                    <li onclick="{prev_page}" class="previous disabled"><a ><span aria-hidden="true">&larr;</span> Previous</a></li>
                    <li onclick="{next_page}" class="next disabled"><a >Next <span aria-hidden="true">&rarr;</span></a></li>
                </ul>
            </nav>
        </div>
    </div>
    <script>
       import * as actions from "./sectionActions"
       import {load_sections} from "./sectionActions"
       import {globals, $http} from "~/util/util"
       
        var vm = this;
        var update = vm.update
        var state
        var ok = 0
        
         vm.current_faculty = null;
         
      
         
         vm.add_section = function (section) {
            vm.opts.store.dispatch(actions.add_section(section))
         }
         
         vm.delete_section = function (section_id) {
            vm.opts.store.dispatch(actions.delete_section(section_id))
         }
         
        vm.select_faculty = function (current_faculty) {
            vm.state = [];
            vm.current_faculty = current_faculty;
            vm.opts.store.dispatch(load_sections(globals.BASE_API + "/faculty/" + vm.current_faculty + "/section?with=language&client_id=" + globals.client_id));
            vm.state = vm.opts.store.getState().sections;
            vm.update();
        }
            
        vm.on("update", function () {
            if (vm.opts.store && ok == 0) {
                vm.opts.store.subscribe(function () {
                    update()
                }).bind(vm)
            }
            
            if (vm.opts.store ) {
                ok = 1
                vm.state = vm.opts.store.getState().sections;
            }

            $(".pager").hide();
            if(vm.state && vm.state.sections && vm.state.sections.data){
                if(vm.state.sections.data.length == 0) {
                    $(".pager").hide();
                    return;
                }
                $(".pager").show();
                if(vm.state.sections.next_page_url != null){
                    $(".next").removeClass("disabled");
                }else{
                    $(".next").addClass("disabled");
                }
            
                if(vm.state.sections.prev_page_url != null){
                    $(".previous").removeClass("disabled");
                }else{
                    $(".previous").addClass("disabled");
                }
                
            }
        });
        
        vm.next_page = function(event){
            if(vm.state){
                if(vm.state.sections.next_page_url != null){
                    $(event.target).removeClass("disabled")
                    vm.opts.store.dispatch(load_sections(vm.state.sections.next_page_url + "&client_id="+globals.client_id ));
                    vm.state = vm.opts.store.getState().sections; 
                }else{
                    $(event.target).addClass("disabled")
                }
            }
        }
        
        vm.prev_page = function(event){
            if(vm.state){
                if(vm.state.sections.prev_page_url != null){
                    $(event.target).removeClass("disabled")
                    vm.opts.store.dispatch(load_sections(vm.state.sections.prev_page_url + "&client_id="+globals.client_id ));
                    vm.state = vm.opts.store.getState().sections;
                }else{
                    $(event.target).addClass("disabled")
                }
            }
        }
        
          this.on("unmount", function () {
            this.state.sections = null;
        })
        
    </script>
</section-layout>