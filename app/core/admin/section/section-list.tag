<section-list>
  <div class="section">
        <div class="row">
            <div class="col-md-12">
                <h3 class="alert alert-danger no-filters-message" if={this.opts.data.length == 0}>No sections available for this faculty !</h3>
                <table class="table table-hover table-striped">
                    <tbody>
                        <tr each={this.opts.data} data={ this }>
                            <td>
                               <h4>
                                    <b><p> <span class="icon-book"></span></p> </b>
                               </h4>
                            </td>
                            <td>
                                <h4>
                                    <b>{this.name} {this.language.name}</b>
                                </h4>
                            </td>
                            <td>
                                <a class="btn btn-primary" id="deleteSection" href="#{this.id}" data-toggle="modal" type="button">
                                    <i class="fa fa-fw fa-cog"></i>x
                                </a>
                                <delete-modal data={this} delete-section={"delete_section"}></delete-modal>
                            </td>
                        </tr>                
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section-list>