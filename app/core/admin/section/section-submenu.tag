<section-submenu>
    <div class="submenu">
        <a class="btn btn-primary addSection" data-toggle="modal" href="#addSectionModal" type="button">
            <i class="fa fa-fw fa-cog"></i>+
        </a>
        <add-section add-section={"add_section"} data={this.opts.data} languages={this.opts.languages}/>
    </div>
</section-submenu>