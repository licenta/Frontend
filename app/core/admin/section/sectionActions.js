import {globals, $http} from "~/util/util"

const load_sections = (url)=> {
    return (dispatch, getState)=> {
        $http.get(url)
            .then(function (response) {
                dispatch(sections_loaded(JSON.parse(response).section));
            }).catch((error)=> {
            console.log(error);
        })
    }
}
const sections_loaded = (sections)=> {
    return {
        type: "SECTIONS_LOADED",
        data: sections
    }
}
const delete_section = (data)=> {
    return (dispatch, getState)=> {
        $http.delete(globals.BASE_API + "/section/" + data)
            .then(function (response) {
                dispatch(section_deleted({id:data}));
            }).catch((error)=> {
            console.log(error);
        })
    }
}
const section_deleted = (section)=> {
    return {
        type: "SECTION_DELETED",
        data: section
    }
}
const load_faculties = ()=> {
    return (dispatch, getState)=> {
       $http.get(globals.BASE_API + "/faculty?client_id=" + globals.client_id)
            .then(function (response) {
               
                dispatch(faculties_loaded(JSON.parse(response).faculty.data))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const load_languages = ()=> {
    return (dispatch, getState)=> {
       $http.get(globals.BASE_API + "/language?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(languages_loaded(JSON.parse(response).language))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const languages_loaded = (languages) =>{
    return{
        type: "LANGUAGES_LOADED",
        data: languages
    }
}

const faculties_loaded = (faculty)=> {
    return {
        type: "FACULTIES_LOADED",
        data: faculty
    }
}
const add_section = (data)=> {
    return (dispatch, getState)=> {
        $http.post(globals.BASE_API + "/section",data)
            .then(function (response) {
                console.log(response);
                dispatch(section_added(JSON.parse(response).section));
            }).catch((error)=> {
            var message = JSON.parse(error.responseText).error.name
            console.log(error);
        })
    }
}
const section_added = (section)=> {
    return {
        type: "SECTION_ADDED",
        data: section
    }
}
export {load_sections, delete_section, load_faculties, add_section, load_languages}