const sectionReducer = (state={message:""},action={})=>{
    switch(action.type){
        case 'SECTIONS_LOADED':
            return Object.assign({},state,{sections:action.data});
        case 'SECTION_ADDED':
            if(state.sections.next_page_url === null){
                 var obj = state.sections.data.concat(action.data);
                 state.sections.data = obj;
                 return Object.assign({},state,{sections:state.sections})
            }else{
                 return Object.assign({},state,{sections:state.sections})
            }
        case 'SECTION_DELETED':
            var sectionIndex = state.sections.data.findIndex(function(section){
                return section.id == action.data.id
            })
            
            var obj1 = state.sections.data.slice(0,sectionIndex);
            var obj2 = state.sections.data.slice(sectionIndex + 1);
            state.sections.data = obj1.concat(obj2);
            return Object.assign({},state,{sections: state.sections})
        case 'FACULTIES_LOADED':
            return Object.assign({},state,{faculty:action.data});
        case 'LANGUAGES_LOADED':
             return Object.assign({},state,{languages:action.data});
        default:
            return state
    }
}

export default sectionReducer