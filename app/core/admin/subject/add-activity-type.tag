<add-activity-type>
    <div class="modal fade" id="addActivityType" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <ul class="nav nav-tabs" id="tabContent">
                        <li class="active add-tab"><a href="#add-{this.opts.id}" data-toggle="tab">Add New Activity</a></li>
                        <li class="delete-tab"><a href="#delete-{this.opts.id}" data-toggle="tab">Delete Activity</a></li>
                    </ul>
                </div>
                <div class="modal-body row">
                    <form id="addAct">
                        <div class="tab-content">
                            <div class="tab-pane active add" id="add-{this.opts.id}">
                                <div class="col-md-12 col-xs-12">
                                    <label>Hour Count:</label>
                                    <p><input type="text" name="hour" id="hour" placeholder="Hour Count" style="border-radius:4px;width:100%"></p>
                                </div>
                                <div class="col-xs-12 col-md-6 select-container">
                                    <label>Available Activities:</label>
                                    <select class="selectpicker show-tick btn-labeled" name="activity_available" data-live-search="true" style="display: none;" data-style="btn-success">
                                        <option each="{this.opts.available_activities}" value="{id}">
                                            {name}                                
                                        </option>
                                    </select>
                                </div>
                            </div>
                                
                            <div class="tab-pane delete" id="delete-{this.opts.id}">
                                <div class="col-xs-12 col-md-6 select-container">
                                    <label>Choose Activity to Delete:</label>
                                    <select class="selectpicker show-tick btn-labeled" name="current_activity" data-live-search="true" style="display: none;" data-style="btn-success">
                                        <option each="{this.opts.current_activities}" value="{subject_act_id}">
                                            {name}                                
                                        </option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button"
                                onclick="{execute_action}"><span class="glyphicon glyphicon-check"></span> Save Changes
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <script>
        var vm = this;
        this.on('updated', function() {
            $('.selectpicker').selectpicker('refresh');
            
         })
         
         this.on('mount', function(){
           $("#addAct").validate({
               rules:{
                   hour:{
                       required: true,
                       digits: true
                   }
               },
               messages:{
                   hour:"Please specify a number of hours for this activity."
               },
               submitHandler: function() {
                       if($('#add-' + vm.opts.id).hasClass('active')){
                            var newActivity = {
                            "hour_count": vm.hour.value,
                            "subject_id": vm.opts.id,
                            "activity_type_id": vm.activity_available.value,
                            }
                            
                            vm.opts.add_activity(newActivity);
                            vm.hour.value = null;
                        }else{
                            vm.opts.delete_activity(vm.current_activity.value, vm.opts.id);
                        }
                        
                        $("#addActivityType").modal('hide');
                }
           })
          
        });
        
        this.execute_action = function(){
             $("#addAct").submit();
        }
    </script>
</add-activity-type>