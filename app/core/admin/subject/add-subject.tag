<add-subject>
    <div class="modal fade" id="addSubjectModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Add a new Subject</h4>
                </div>
                <div class="modal-body row">
                    <form id="subjectAdd">
                        <div class="col-md-12 col-xs-12">
                            <label>Subject Name:</label>
                            <p><input type="text" name="name" id="name" placeholder="Subject Name" style="border-radius:4px;width:100%" ></p>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Semester:</label>
                            <select class="selectpicker show-tick btn-labeled" name="semester" data-live-search="true" style="display: none;" data-style="btn-success">
                                <option each="{this.opts.semesters}" value="{id}">
                                    {parity}                                
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Discipline Type:</label>
                            <select class="selectpicker show-tick" name="distype" data-live-search="true" onchange={update_type} style="display: none;"  data-style="btn-success">
                                <option each="{this.opts.discipline_types}" value="{id}">
                                    {name}                                
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Examination Type:</label>
                            <select class="selectpicker show-tick btn-labeled" name="examtype" data-live-search="true" onchange="{loadDep}" style="display: none;" data-style="btn-success">
                                <option each="{this.opts.examination_types}" value="{id}" >
                                    {name}
                                </option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button"
                                onclick="{submit_form}" ><span class="glyphicon glyphicon-check"></span> Add Subject
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <script>
        var vm = this
        vm.professorSelected = false;
        
        vm.submit_form = function(){
            $("#subjectAdd").submit();
        }
        
        vm.on('mount', function(){
           $("#subjectAdd").validate({
               rules:{
                   name:"required",
               },
               messages:{
                   name:"Please specify a subject name."
               },
                submitHandler: function() {
                        var newSubject = {
                        "name": vm.name.value,
                        "semester_id": vm.semester.value,
                        "curriculum_id": vm.opts.current_curriculum,
                        "discipline_type_id": vm.distype.value,
                        "examination_type_id": vm.examtype.value,
                    };
                    
                    vm.opts.add_subject(newSubject); 
                    vm.name.value = null;
                    $("#addSubjectModal").modal('hide');
                }
           })
          
        });
        
        vm.on('updated', function() {
            $('.selectpicker').selectpicker('refresh');
        });
    </script>
</add-subject>