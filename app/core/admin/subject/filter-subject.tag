<filter-subject>
    <div class="auto-select row">
        <div class="col-xs-12 col-sm-6 col-md-3 select-container">
            <label>Faculty:</label>
            <select class="selectpicker show-tick btn-labeled" data-live-search="true" style="display: none;" onchange="{select_fac}"  data-style="btn-success">
                <option value="-1">Nothing selected</option>
                <option each="{this.opts.faculties}" value="{id}">
                    {name}
                </option>
            </select>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-3 select-container">
            <label>Section:</label>
            <select class="selectpicker show-tick" data-live-search="true"  style="display: none;" onchange="{select_sec}"  data-style="btn-success">
                <option value="-1">Nothing selected</option>
                <option each="{this.opts.sections}" value="{id}" selected="{props.current_section==id}">
                    {name} - {language.name}
                </option>
            </select>
        </div>
        <div class="col-xs-12  col-sm-6 col-md-3 select-container">
            <label>Year:</label>
            <select class="selectpicker show-tick" data-live-search="true" name="year" style="display: none;" onchange="{this.opts.select_year}" data-style="btn-success">
                <option value="-1">Nothing selected</option>
                <option each="{this.opts.years}" value="{id}" selected="{props.current_year==id}">
                    {start_year} - {end_year}
                </option>
            </select>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 select-container">
            <label>Semester:</label>
            <select class="selectpicker show-tick" style="display: none;" onchange="{this.opts.select_semester}" data-style="btn-success">
                <option selected="{props.current_semester==-1}" value="{-1}">All</option>
                <option each="{this.opts.semesters}" value="{id}" selected="{props.current_semester==id}">
                    Semester - {parity}
                </option>
            </select>
        </div>
    </div>
    
    <script>
        this.on('updated', function() {
            $('.selectpicker').selectpicker('refresh');
        })   
        
        this.select_fac = function(event){
            this.year.value = -1;
            this.opts.select_faculty(event);
        }
        
        this.select_sec = function(event){
            this.year.value = -1;
            this.opts.select_section(event);
        }   
    </script>
</filter-subject>