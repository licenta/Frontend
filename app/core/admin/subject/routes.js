import "./subject-list.tag"
import "./subject-admin.tag"
import "./subject-view.tag"
import "./subject-layout-container.tag"
import "./filter-subject.tag"
import "./add-subject.tag"
import "./add-activity-type.tag"
import "./update-subject.tag"

import riot from "riot"

import {load_subjects,load_faculties,load_activity_types, load_discipline_types,load_examination_types} from "./subjectActions"

import {reduxStore} from "~/reduxStore"


const states = [{
    name: 'layout.subject.admin',
    route: '/admin',
    template: "subject-view",
    activate: (context)=> {
        var tag = context.domApi
        tag.opts.store = reduxStore
        riot.mount("view", "subject-layout-container", {store: reduxStore})
        tag.opts.store.dispatch(load_faculties());
        tag.opts.store.dispatch(load_activity_types());
        tag.opts.store.dispatch(load_discipline_types());
        tag.opts.store.dispatch(load_examination_types());
        tag.update()
        
        context.on('destroy', function () {
            tag.unmount()
        })
    }
}]

export default states