<subject-admin>
    <div class="tile">
        <div class="department">
            <p if="{displayed_activity.length != 0}"><span class="label label-default activity-type-span" onclick="{add_activity_type}">{displayed_activity}</span></p>
            <p if="{displayed_activity.length == 0}" ><span  class="label label-default activity-type-span" onclick="{add_activity_type}"
                >Add Activity type</span></p>
            <button class="delete-btn" onclick="{delete_subject}">X</button>
        </div>
        <div class="line"></div>
        <div class="title" onclick="{open_update_modal}">
            <p>{this.opts.name}</p>
            
        </div>
        <div class="details" style="background-color: #EA6D4E">
            <p><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Teacher name</p>
        </div>
    </div>
    <script>
        import {globals, $http} from "~/util/util"
        var vm = this;
        vm.displayed_activity = "";
      
        
        this.on("update", function(){
             vm.displayed_activity = "";
        
             $.each(vm.parent.opts.activityTypes, function( index, value) {
                 var id = existsActivity(value.id, vm.opts.subject_activity_type)
                    if(id != -1){
                        vm.displayed_activity += value.name;
                        vm.displayed_activity += " ";
                    }
             });
        });
        
        function existsActivity(activity_id, subject_act_type){
            if(subject_act_type!= undefined){
                for(var i=0; i<=subject_act_type.length - 1;i++){
                    if(subject_act_type[i].activity_type_id == activity_id){
                        return subject_act_type[i].id;
                    }
                } 
            }
            return -1;
        }
        
        this.delete_subject =  function(){
            this.parent.opts.delete_subject(this.opts.id);
        }
        
        this.add_activity_type = function(){
            var current_activities = [];
            var available_activities = [];
           
             var subjectIndex = this.parent.opts.subjects.findIndex(function(subject){
                return subject.id == vm.opts.id
             })
           
             $.each(vm.parent.opts.activityTypes, function( index, value) {
                 var id = existsActivity(value.id, vm.parent.opts.subjects[subjectIndex].subject_activity_type)
                    if(id != -1){
                       var obj = value;
                       $.extend(obj, {"subject_act_id": id});
                        current_activities.push(obj);
                    }else{
                        available_activities.push(value);
                    }
                    
             });
             
             var opts = {
                "id": this.opts.id, 
                "available_activities": available_activities, 
                "add_activity": this.parent.opts.add_activity,
                "current_activities": current_activities,
                "delete_activity" :this.parent.opts.delete_activity
            }
            
            riot.mount('add-activity-type', opts);
            var $currentModal = $("#addActivityType");
            $currentModal.modal('show');
            
            if(current_activities.length == 0){
                $currentModal.find('.delete-tab').addClass('hidden');
            }else{
                $currentModal.find('.delete-tab').removeClass('hidden');
            }
            
            if(available_activities.length == 0){
                $currentModal.find('.add-tab').removeClass('active');
                $currentModal.find('.delete-tab').addClass('active');
                $currentModal.find('.add').removeClass('active');
                $currentModal.find('.delete').addClass('active');
                $currentModal.find('.add-tab').addClass('hidden');
            }else{
                $currentModal.find('.add-tab').addClass('active');
                $currentModal.find('.delete-tab').removeClass('active');
                $currentModal.find('.add').addClass('active');
                $currentModal.find('.delete').removeClass('active');
                $currentModal.find('.add-tab').removeClass('hidden');
            }
             
        }
        
        this.open_update_modal = function(){
            var opts = {
                "id": this.opts.id,
                "examination_types" : this.parent.opts.examination_types, 
                "discipline_types":this.parent.opts.discipline_types,
                "update_subject":this.parent.opts.update_subject,
                "name": this.opts.name, 
                "current_discipline_type" : this.opts.current_discipline_type, 
                "current_examination_type" : this.opts.current_examination_type
            }
            
            riot.mount('update-subject', opts);
            $("#updateSubjectModal").modal('show');
        }
    </script>
</subject-admin>