<subject-layout-container>
    <filter-subject select_faculty="{select_faculty}"
                 current_faculty="{current_faculty}"
                 select_section="{select_section}"
                 current_section="{current_section}"
                 select_year="{select_year}"
                 current_year="{current_year}"
                 current_semester="{current_semester}"
                 select_semester="{select_semester}"
                 faculties="{state.faculties}"
                 sections="{state.sections}"
                 curriculums="{state.curriculums}"
                 years="{state.years}"
                 semesters="{semesters}"/>
    <subject-list subjects="{state.subjects.data}" 
                 activity-types="{state.activity_types.activity_type}" 
                 semesters="{semesters}"
                 discipline_types="{state.discipline_types}"
                 examination_types="{state.examination_types}"
                 add_subject="{add_subject}"
                 current_curriculum = "{current_curriculum}"
                 delete_subject="{delete_subject}"
                 add_activity="{add_activity}"
                 delete_activity="{delete_activity}"
                 update_subject = "{update_subject}"/>
     <nav>
        <ul class="pager">
            <li onclick="{prev_page}" class="previous disabled"><a ><span aria-hidden="true">&larr;</span> Previous</a></li>
            <li onclick="{next_page}" class="next disabled"><a >Next <span aria-hidden="true">&rarr;</span></a></li>
        </ul>
    </nav>
    <script>
    import {load_sections, load_curriculums, load_subjects, load_years, load_semesters, add_subject, delete_subject
         ,add_activity, delete_activity, update_subject} from "./subjectActions"
    import {globals} from "~/util/util"
    
    var store = this.opts.store
        var update = this.update
        var state
        var ok = 0
       
        
        var vm = this
        vm.current_curriculum = null;
        this.current_year = -1;
        this.current_faculty = -1;
        this.current_section = -1;
        this.current_semester = -1;
        this.semesters = [];
        
        this.select_faculty = function (event) {
            this.current_faculty = event.target.value;
            this.semesters = [];
            if(vm.state.subjects){
                vm.state.subjects.data = null;
            }
            if(this.current_faculty != -1){
                store.dispatch(load_sections("/faculty/" + this.current_faculty + "/section?with=Language"))
            }
        }.bind(this)
        
        this.select_section = function (event) {
            this.current_section = event.target.value;
            this.semesters = [];
            if(vm.state.subjects != undefined){
                vm.state.subjects.data = null;
                vm.update();
            }
            
            if(this.current_faculty != -1 && this.current_section != -1){
                store.dispatch(load_curriculums("/section/" + this.current_section + "/curriculum"));
            }
            
        }.bind(this)
        
        this.select_year = function (event, section) {
            if(this.current_faculty == -1 || this.current_section == -1){
                if(vm.state.subjects){
                    vm.state.subjects.data = null;
                    vm.update();
                }
                return;
            }
            this.current_year = event.target.value;
            this.semesters = [];
            load_view_years(this.current_year)
        }.bind(this)

        function load_view_years(year_id){
            var myYear;
           if(vm.current_year != -1){
               vm.state.years.forEach(function(year){
                   if(year.id == year_id){
                       myYear = year;
                       vm.semesters = year.semester;
                       return
                   }
               })
               
            var curriculums = store.getState().subject.curriculums
            if (curriculums)
                curriculums.forEach(function (curriculum) {
                    if (curriculum.first_semester_id == myYear.semester[0].id) {
                        vm.current_curriculum = curriculum.id
                        if (vm.current_semester == -1)
                            store.dispatch(load_subjects(globals.BASE_API + "/curriculum/" + curriculum.id + "/subject?with=SubjectActivityType"))
                        else{
                            store.dispatch(load_subjects(globals.BASE_API + "/curriculum/" + curriculum.id + "/subject" + "?semester_id=" + vm.current_semester + "?with=SubjectActivityType"))
                        }
                        return
                    }
                    return
                })
           }
        }
        
        this.select_semester = function (event) {
            this.current_semester = event.target.value;
            this.current_section = event.target.value;
            if(this.current_faculty == -1 || this.current_section == -1){
                vm.state.subjects.data = null;
                vm.update();
                return;
            }else{
                 if (this.current_semester == -1)
                    store.dispatch(load_subjects(globals.BASE_API + "/curriculum/" + vm.current_curriculum + "/subject?with=SubjectActivityType"))
                else{
                    store.dispatch(load_subjects(globals.BASE_API + "/curriculum/" + vm.current_curriculum + "/subject" + "?semester_id=" + this.current_semester + "?with=SubjectActivityType"))
                }
            }
        }.bind(this)
        
        this.add_subject= function(subject){
            store.dispatch(add_subject(subject));
        }
        
        this.delete_subject = function(id){
            store.dispatch(delete_subject(id));
        }
        
        this.add_activity = function(activity){
             store.dispatch(add_activity(activity));
             vm.update();
        }
        
        this.delete_activity = function(activity_id, subject_id){
            store.dispatch(delete_activity(activity_id, subject_id));
            vm.update();
        }
        
        this.update_subject = function(subject, subject_id){
            store.dispatch(update_subject(subject,subject_id));    
        }
        
        
        this.on("mount", function (){
            store.dispatch(load_years("/academic-year?with=Semester"));
        });
        
        this.on("update", function () {
             if (store && ok == 0) {
                store.subscribe(function () {
                    update()
                }).bind(this)
            }
            
            if (store) {
                ok = 1
                this.state = store.getState().subject;
            }
           
            
            $(".pager").hide();
            
            if(this.state){
                if(this.state.subjects && this.state.subjects.data){
                    if(this.state.subjects.data.length == 0) {
                        $(".pager").hide();
                        return;
                    }
                    
                    $(".pager").show();
                    if(this.state.subjects.next_page_url != null){
                      $(".next").removeClass("disabled");
                    }else{
                      $(".next").addClass("disabled");
                    }
                
                    if(this.state.subjects.prev_page_url != null){
                        $(".previous").removeClass("disabled");
                    }else{
                        $(".previous").addClass("disabled");
                    }
                }
            }
        })
        
         vm.next_page = function(event){
            if(vm.state){
                if(vm.state.subjects.next_page_url != null){
                    $(event.target).removeClass("disabled")
                    store.dispatch(load_subjects(vm.state.subjects.next_page_url + "&with=SubjectActivityType"  + "&client_id="+globals.client_id ));
                    vm.state = store.getState().subject; 
                     vm.update();
                }else{
                    $(event.target).addClass("disabled")
                }
            }
        }
        
        vm.prev_page = function(event){
            if(vm.state){
                if(vm.state.subjects.prev_page_url != null){
                    $(event.target).removeClass("disabled")
                    store.dispatch(load_subjects(vm.state.subjects.prev_page_url + "&with=SubjectActivityType" + "&client_id="+globals.client_id ));
                    vm.state = store.getState().subject;
                    vm.update();
                }else{
                    $(event.target).addClass("disabled")
                }
            }
        }
        
    </script>
</subject-layout-container>