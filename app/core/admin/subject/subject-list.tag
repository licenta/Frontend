<subject-list>
    <div id="list-container">
        <h3 class="alert alert-danger no-filters-message" if="{filterNotOk}">Filter options not ok!</h3>
        <h3 class="alert alert-warning no-filters-message" if="{!filterNotOk && subjectsNotFount}">No data for this search !</h3>

        <div class="tile-container" if="{!(filterNotOk || subjectsNotFount)}">
             <div class="tile add-course" onclick="{open_modal}">
                <div class="department">
                    <p ><span  class="label label-default">Add new Subject</span></p>
                </div>
                <div class="line"></div>
                <div class="title">
                    <p>+</p>
                </div>
                <div class="details" style="background-color: #EA6D4E">
                </div>
            </div>
            <add-subject discipline_types="{this.opts.discipline_types}" 
                        semesters="{this.opts.semesters}"
                        examination_types="{this.opts.examination_types}" 
                        add_subject="{this.opts.add_subject}"
                        current_curriculum ="{this.opts.current_curriculum}"></add-subject>
        </div>
        <div if="{filterNotOk == false}"  class="tile-container" each="{this.opts.subjects}">
            <subject-admin name="{name}" id="{id}" 
                            activity-types="{this.opts.activity-types}" 
                            subject_activity_type="{subject_activity_type}" 
                            delete_subject="{this.opts.delete_subject}"
                            add_activity="{this.opts.add_activity}" 
                            delete_activity="{this.opts.delete_activity}"
                            current_discipline_type="{discipline_type_id}" 
                            current_examination_type="{examination_type_id}">
            </subject-admin>
            <!--<update-subject dataid="{id}" 
                        examination_types="{examination_types}" 
                        discipline_types="{this.opts.discipline_types}"
                        update_subject="{this.opts.update_subject}"
                        name="{name}" 
                        current_discipline_type="{discipline_type_id}" 
                        current_examination_type="{examination_type_id}" >
            </update-subject>-->
        </div>
        <add-activity-type></add-activity-type>
        <update-subject></update-subject>
    </div>
    <script>
        this.filterNotOk = true;
        this.subjectsNotFount = true;
        this.disciplines = [];
        this.examinations = [];
        
        this.open_modal = function(){
            if(!this.filterNotOk){
                $("#addSubjectModal").modal('show');  
            }
        }
        
       
        this.on("update", function(){
            //this.disciplines= this.opts.discipline_types;
           // this.examinations = this.opts.examination_types;
            this.filterNotOk = true;
            
            if(this.opts.subjects){
                this.filterNotOk = false;
                if(this.opts.subjects.length != 0) {
                    this.subjectsNotFount = false;
                }
                else {
                    this.subjectsNotFount = true;
                }
            }
        });

        var tileWidth = 250;
        var tileMinDistance = 20;

        var $style = $('<style>', {type: 'text/css'});
        $("body").append($style);

        var width = -1;

        var resizeFunc = function(){
            var newWidth = $('#list-container').innerWidth();
            if (newWidth == width) {
                return;
            }
            width = newWidth;
            if(!width) {
                setTimeout(resizeFunc, 100);
                return;
            }


            var noOfTiles = parseInt(width / (tileWidth + tileMinDistance));
            var rest = width - noOfTiles * (tileWidth + tileMinDistance);
            var padding = rest / noOfTiles / 2;

            $style.html('.tile-container{padding: 0 ' + padding + 'px;}');
        }

        $(window).resize(resizeFunc);
        setInterval(resizeFunc, 1000);
        resizeFunc();
    </script>
</subject-list>