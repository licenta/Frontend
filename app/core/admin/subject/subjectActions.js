import {globals, $http} from "~/util/util"

const load_faculties = ()=> {
    return (dispatch, getState)=> {
        $http.get(globals.BASE_API + "/faculty?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(faculties_loaded(JSON.parse(response).faculty.data))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const faculties_loaded = (facultys)=> {
    return {
        type: "FACULTIES_LOADED",
        data: facultys
    }
}

const load_discipline_types = ()=> {
    return (dispatch, getState)=> {
        $http.get(globals.BASE_API + "/discipline-type?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(discipline_types_loaded(JSON.parse(response).discipline_type))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const discipline_types_loaded = (discipline_types)=>{
    return{
        type:'DISCIPLINE_TYPES_LOADED',
        data:discipline_types
    }
}

const load_examination_types = ()=> {
    return (dispatch, getState)=> {
        $http.get(globals.BASE_API + "/examination-type?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(examination_types_loaded(JSON.parse(response).examination_type))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const examination_types_loaded = (examination_types)=>{
    return{
        type:'EXAMINATION_TYPES_LOADED',
        data:examination_types
    }
}

const load_sections = (url,onsuccess=null)=> {
    return (dispatch, getState)=> {
        var query_param = "&"
        if(url.indexOf('?') === -1)
        {
            query_param = "?"
        }
        $http.get(globals.BASE_API + url + query_param + "client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(sections_loaded(JSON.parse(response).section.data))
                if(onsuccess)
                    onsuccess()
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const sections_loaded = (sections)=> {
    return {
        type: "SECTIONS_LOADED",
        data: sections
    }
}

const load_curriculums = (url,onsuccess=null)=> {
    return (dispatch, getState)=> {
        var query_param = "&"
        if(url.indexOf('?') == -1)
        {
            query_param = "?"
        }
        $http.get(globals.BASE_API + url + query_param + "client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(load_curriculums_loaded(JSON.parse(response).curriculum.data))
                if(onsuccess){
                    onsuccess()
                }
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const load_curriculums_loaded = (curriculums)=> {
    return {
        type: "CURRICULUMS_LOADED",
        data: curriculums
    }
}

const load_subjects = (url)=> {
    return (dispatch, getState)=> {
        $http.get( url )
            .then(function (response) {
                localStorage.setItem("subject", response);
                dispatch(subjects_loaded(JSON.parse(response).subject))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const subjects_loaded = (subjects)=> {
    return {
        type: "SUBJECTS_LOADED",
        data: subjects
    }
}

const load_years = (url,onsuccess=null) => {
    return (dispatch, getState)=> {
        var query_param = "&"
        if(url.indexOf('?') === -1)
        {
            query_param = "?"
        }
        $http.get(globals.BASE_API + url + query_param + "client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(years_loaded(JSON.parse(response).academic_year.data))
                if(onsuccess){
                    onsuccess(JSON.parse(response).academic_year.data)
                }
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const years_loaded = (years) => {
    return {
        type: "YEARS_LOADED",
        data: years
    }
}

const load_semesters = (url) => {
    return (dispatch, getState)=> {
        var query_param = "&"
        if(url.indexOf('?') === -1)
        {
            query_param = "?"
        }

        $http.get(globals.BASE_API + url + query_param + "client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(semesters_loaded(JSON.parse(response).semester))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const semesters_loaded = (years) => {
    return {
        type: "SEMESTERS_LOADED",
        data: years
    }
}

const load_activity_types = () => {
    return (dispatch, getState)=> {
        $http.get(globals.BASE_API +"/activity-type?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(activity_types_loaded(JSON.parse(response)))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const activity_types_loaded = (activity_types) => {
    return {
        type: "ACTIVITY_TYPES_LOADED",
        data: activity_types
    }
}

const add_subject = (data) =>{
    return (dispatch, getState)=> {
        $http.post(globals.BASE_API + "/subject", data)
            .then(function (response) {
                dispatch(subject_added(JSON.parse(response).subject));
            }).catch((error)=> {
            var message = JSON.parse(error.responseText).error.name
            console.log(error);
        })
    }
}

const subject_added = (subjects)=> {
    return {
        type: "SUBJECT_ADDED",
        data: subjects
    }
}

const delete_subject = (data, subject_id)=> {
    return (dispatch, getState)=> {
        $http.delete(globals.BASE_API + "/subject/" + data)
            .then(function (response) {
                dispatch(subject_deleted({id:data,s_id:subject_id}));
            }).catch((error)=> {
            console.log(error);
        })
    }
}

const subject_deleted = (subject)=> {
    return {
        type: "SUBJECT_DELETED",
        data: subject
    }
}

const add_activity = (data) =>{
    return (dispatch, getState)=> {
        $http.post(globals.BASE_API + "/subject-activity-type", data)
            .then(function (response) {
                dispatch(activity_added(JSON.parse(response).subject_activity_type));
            }).catch((error)=> {
                console.log(error)
                var message = JSON.parse(error.responseText).error.name
                console.log(error);
        })
    }
}

const activity_added = (activity_types)=> {
    return {
        type: "ACTIVITY_TYPE_ADDED",
        data: activity_types
    }
}

const delete_activity = (data, subject_id) =>{
    return (dispatch, getState)=> {
        $http.delete(globals.BASE_API + "/subject-activity-type/" + data)
            .then(function (response) {
                dispatch(activity_deleted({id:data,s_id:subject_id}));
            }).catch((error)=> {
                var message = JSON.parse(error.responseText).error.name
            console.log(error);
        })
    }
}

const activity_deleted = (activity_types)=> {
    return {
        type: "ACTIVITY_TYPE_DELETED",
        data: activity_types
    }
}

const update_subject = (data, id) => {
    return (dispatch, getState) =>{
        $http.put(globals.BASE_API + "/subject/" + id +"?client_id=" + globals.client_id, data)
            .then(function(response){
                dispatch(subject_updated(JSON.parse(response)));
            }).catch((error) => {
                var message = JSON.parse(error.responseText).error.name;
                console.log(error);
            })
    }
}

const subject_updated = (subject) =>{
    return{
        type:"SUBJECT_UPDATED",
        data: subject
    }
}

export {load_subjects, load_faculties, load_sections, load_curriculums, load_years, load_semesters, load_activity_types,
    load_discipline_types, load_examination_types, add_subject, delete_subject, add_activity, delete_activity, update_subject}