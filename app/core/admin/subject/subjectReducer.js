const subjectReducer = (state={title:"Default title"},action={})=>{
    switch(action.type){
        case 'SUBJECTS_LOADED':
            return Object.assign({},state,{subjects:action.data})
        case 'FACULTIES_LOADED':
            return Object.assign({},state,{faculties:action.data})
        case 'SECTIONS_LOADED':
            return Object.assign({},state,{sections:action.data})
        case 'CURRICULUMS_LOADED':
            return Object.assign({},state,{curriculums:action.data})
        case 'YEARS_LOADED':
            return Object.assign({},state,{years:action.data})
        case 'SEMESTERS_LOADED':
            return Object.assign({},state,{semesters:action.data})
        case 'USERS_LOADED':
            return Object.assign({},state,{users:action.data});
        case 'DEPARTMENTS_LOADED':
            return Object.assign({},state,{departments:action.data});
        case "ACTIVITY_TYPES_LOADED":
            return Object.assign({},state,{activity_types:action.data});
        case 'DISCIPLINE_TYPES_LOADED':
            return Object.assign({},state,{discipline_types:action.data});
        case 'EXAMINATION_TYPES_LOADED':
            return Object.assign({},state,{examination_types:action.data});
        case 'SUBJECT_ADDED':
            if(state.subjects.next_page_url === null){
                 $.extend(action.data,{subject_activity_type:[]});
                 var obj = state.subjects.data.concat(action.data);
                 state.subjects.data = obj;
                 return Object.assign({},state,{subjects: state.subjects})
            }else{
                 return Object.assign({},state,{subjects:state.subjects})
            }
        case 'SUBJECT_DELETED':
            var subjectIndex = state.subjects.data.findIndex(function(subject){
                return subject.id == action.data.id
            })
            
            var obj1 = state.subjects.data.slice(0,subjectIndex);
            var obj2 = state.subjects.data.slice(subjectIndex + 1);
            state.subjects.data = obj1.concat(obj2);
            return Object.assign({},state,{subjects: state.subjects})
        case "ACTIVITY_TYPE_ADDED":
            var subject_id = action.data.subject_id;
            $.each(state.subjects.data, function(index, value){
                if(value.id == subject_id){
                     state.subjects.data[index].subject_activity_type.push(action.data);
                     return Object.assign({}, state, {subjects: state.subjects})
                }
            })
            return Object.assign({}, state, {subjects: state.subjects})
        case "ACTIVITY_TYPE_DELETED":
            $.each(state.subjects.data, function(index, value){
                if(value.id == action.data.s_id){
                    var activityIndex = value.subject_activity_type.findIndex(function(subject_activity_type){
                        return subject_activity_type.id == action.data.id
                    });
                    
                    if(activityIndex != -1){
                        var obj1 = state.subjects.data[index].subject_activity_type.slice(0,activityIndex);
                        var obj2 = state.subjects.data[index].subject_activity_type.slice(activityIndex + 1);
                        state.subjects.data[index].subject_activity_type = obj1.concat(obj2);
                        return false;
                    }
                    return false;
                }  
            });
            return Object.assign({},state,{subjects: state.subjects})
          case 'SUBJECT_UPDATED':
               subjectIndex = state.subjects.data.findIndex(function(subject){
                return subject.id == action.data.subject.id
               })
              
                state.subjects.data[subjectIndex].name = action.data.subject.name;
                state.subjects.data[subjectIndex].discipline_type_id = action.data.subject.discipline_type_id;
                state.subjects.data[subjectIndex].examination_type_id = action.data.subject.examination_type_id;
                return Object.assign({},state,{subjects: state.subjects})
        default:
            return state
    }
}
export default subjectReducer