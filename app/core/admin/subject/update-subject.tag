<update-subject>
	<div class="modal fade" id="updateSubjectModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
					<h4 class="modal-title" style="float:left">Update Subject</h4>
                </div>
                <div class="modal-body row">
                    <form id="updSub">
                        <div class="col-md-12 col-xs-12">
                            <label>Subject Name:</label>
                            <p><input type="text" name="name" onchange="{update_input}" value="{value}" id="name" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Discipline Type:</label>
                            <select class="selectpicker show-tick" name="distype" data-live-search="true" style="display: none;" data-style="btn-success"
                                value={this.opts.current_discipline_type}>
                                <option each="{this.opts.discipline_types}" value="{id}">
                                    {name}                                
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Examination Type:</label>
                            <select class="selectpicker show-tick btn-labeled" name="examtype" data-live-search="true" onchange="{loadDep}" style="display: none;" data-style="btn-success"
                                value={this.opts.current_examination_type}>
                                <option each="{this.opts.examination_types}" value="{id}" >
                                    {name}
                                </option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button"
                                onclick="{submit_form}" ><span class="glyphicon glyphicon-check"></span> Save Changes
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    <script>
        this.value = this.opts.name;
        var vm = this;
        
        this.on('updated', function() {
            $('.selectpicker').selectpicker('refresh');
            
         });
        
        this.on('mount', function(){
           $("#updSub").validate({
               rules:{
                   name:"required"
               },
               messages:{
                   name:"Please specify a name for this subject."
               },
               submitHandler: function() {
                       var newSubj = {
                            "name": vm.value,
                            "discipline_type_id": vm.distype.value,
                            "examination_type_id": vm.examtype.value,
                        }
                        
                        vm.opts.update_subject(newSubj, vm.opts.id);
                        $("#updateSubjectModal").modal('hide');
                }
           })
          
        });
        
        this.submit_form = function(){
            $("#updSub").submit();
        }
        
        this.update_input = function(e){
            this.value = e.target.value;
            this.update();
        }
    </script>      
</update-subject>