<add-user>
    <div class="modal fade" id="addUserModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Add a new User</h4>
                </div>
                <div class="modal-body row">
                    <form id="addUsr">
                        <div class="col-md-12 col-xs-12">
                            <label>User Name:</label>
                            <p><input type="text" name="name" id="name" placeholder="User Name" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>E-mail:</label>
                            <p><input type="email" name="email" id="email" placeholder="E-mail" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>First Name:</label>
                            <p><input type="text" name="firstname" id="firstname" placeholder="First Name" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>Last Name:</label>
                            <p><input type="text" name="lastname" id="lastname" placeholder="Last Name" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Role:</label>
                            <select class="selectpicker show-tick btn-labeled" name="role" data-live-search="true" style="display: none;" data-style="btn-success">
                                <option each="{this.opts.roles}" value="{id}">
                                    {name}                                
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Type:</label>
                            <select class="selectpicker show-tick" name="type" data-live-search="true" onchange={update_type} style="display: none;"  data-style="btn-success">
                                <option>student</option>
                                <option>professor</option>
                            </select>
                        </div>
                        <div class="col-md-12 col-xs-12" if="{!professorSelected}">
                            <label>Registration No:</label>
                            <p><input type="text" name="regNo" id="regNo" placeholder="Registration No" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div if="{professorSelected}" class="col-xs-12 col-md-6 select-container" >
                            <label>Professor Type:</label>
                            <select class="selectpicker show-tick" name="proftype" data-live-search="true"  style="display: none;"  data-style="btn-success">
                                <option>doctorand</option>
                                <option>asistent</option>
                                <option>lector</option>
                                <option>profesor universitar</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Faculty:</label>
                            <select class="selectpicker show-tick btn-labeled" name="faculty" data-live-search="true" onchange="{loadDep}" style="display: none;" data-style="btn-success">
                                <option each="{this.opts.faculty}" value="{id}" >
                                    {name}
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-6 select-container">
                            <label>Department:</label>
                            <select class="selectpicker show-tick btn-labeled" name="department" data-live-search="true" style="display: none;" data-style="btn-success">
                                <option each="{this.opts.departments}" value="{id}" >
                                    {name}
                                </option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-success btnYes" type="button"
                                onclick="{submit_form}"><span class="glyphicon glyphicon-check"></span> Add User
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>

    <script>
       //photo is the default one.
       //each user can change its photo and its pass by it's own
        import {globals, $http} from "~/util/util"
        var vm = this
        vm.professorSelected = false;
       var rules;
       var messages;
        this.on('updated', function() {
            $('.selectpicker').selectpicker('refresh');
        })
        
        this.submit_form = function(){
            $("#addUsr").submit();
        }
        
        this.update_type = function(){
            if(this.type.value == "professor"){
                vm.professorSelected = true;
                $( "#regNo" ).rules( "remove" );
            }else{
                vm.professorSelected = false; 
            }
        }
        
         vm.on('mount', function(){
              rules = {
                   name:{
                       required: true,
                       minlength: 2
                   },
                   email:{
                       required: true,
                       email: true
                   },
                   firstname: {
                       required: true,
                       minlength: 2
                   },
                   lastname:  {
                       required: true,
                       minlength: 2
                   },
                   regNo:{
                        required: true,
                        digits: true
                   }
              }
              
               messages = {
                  name:"User name is required",
                  email:"A valid email address is required",
                  firstname:"First name is required and must have at least 2 characters",
                  lastname:"Last name is required and must have at least 2 characters",
                  regNo: "A valid registration number is required"
              }
             
            $("#addUsr").validate({
                rules: rules,
                messages:messages,
                    submitHandler: function() {
                        if(vm.professorSelected){
                                var newUser = {
                                    "name": vm.name.value,
                                    "password": generatePassword() ,
                                    "email": vm.email.value,
                                    "first_name": vm.firstname.value,
                                    "last_name": vm.lastname.value,
                                    "role_id": vm.role.value,
                                    "type": vm.type.value,
                                    "professor_type": vm.proftype.value,
                                    "department_id": vm.department.value,
                                }
                            }else{
                                var newUser = {
                                    "name": vm.name.value,
                                    "password": generatePassword(),
                                    "email": vm.email.value,
                                    "first_name": vm.firstname.value,
                                    "last_name": vm.lastname.value,
                                    "role_id": vm.role.value,
                                    "type": vm.type.value,
                                    "registration_number":vm.regNo.value
                                }
                            }
                            
                            vm.parent.opts.add_user(newUser);
                            vm.name.value = null;
                            vm.firstname.value = null;
                            vm.lastname.value = null;
                            vm.email.value = null;
                            vm.regNo.value = null;
                            $("#addUserModal").modal('hide');
                    }
            })
        });
        
        this.loadDep = function(){
            vm.parent.opts.loadDepartments(this.faculty.value);
        }
        
        function generatePassword() {
           //for now it's just a generated pass
           //a user can change it.
            var length = 8,
                charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                retVal = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                retVal += charset.charAt(Math.floor(Math.random() * n));
            }
            return retVal;
        }
        
    </script>
</add-user>