<delete-user>
    <div class="modal fade" id="{this.opts.data.id}" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Delete User</h4>
                </div>
                <div class="modal-body row">
                    <h5>    Are you sure you want to delete user {this.opts.data.name}?</h5>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button"
                                onclick="{delete_user}" data-dismiss="modal"><span class="glyphicon glyphicon-check"></span> Delete User
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <script>
        var vm = this; 
        
        this.delete_user = function(){
            var id = this.opts.data.id;
            
            vm.parent.parent.opts.delete_user(id);
        }
    </script>
</delete-user>