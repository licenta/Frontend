<filter-users>
    <div class="filter-container">
        <div class="filter">
            <div class="input-group">
                <span class="input-group-btn">
                    <input type="hidden" name="search_param" value="all" id="search_param" >
                    <input type="text" class="form-control" name="x" placeholder="Search ..." onkeyup={search_term}>
                </span>
            </div>
        </div>
    </div>
    <script>
        var vm = this;
        
        this.search_term = function(){
            var term = this.x.value;
            vm.opts.search(term);
        }
        
        this.clear_search = function(){
            vm.opts.clear_search();
            this.x.value = "";
        }
    </script>
</filter-users>