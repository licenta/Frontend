import "./user-layout.tag"
import "./filter-users.tag"
import "./user-list.tag"
import "./user-admin.tag"
import "./add-user.tag"
import "./delete-user.tag"


import {load_users, load_faculties, load_roles} from "./userActions"
import {globals} from "~/util/util"
import {reduxStore} from "~/reduxStore"


const states = [{
    name: 'layout.user',
    route: '/users',
    template: 'user-layout',
    activate: (context)=> {
        var tag = context.domApi;
        tag.opts.store = reduxStore;
        tag.opts.store.dispatch(load_users(globals.BASE_API + "/user"));
        tag.opts.store.dispatch(load_faculties());
        tag.opts.store.dispatch(load_roles());
        tag.update();
    }
}]

export default states