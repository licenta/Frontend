<user-admin>
        <div class="span12">
            <ul class="thumbnails">
                <li class="span5 clearfix" onclick="{update_user}">
                    <div class="thumbnail clearfix">
                    <img src="{url}" alt="ALT NAME" class="pull-left span2 clearfix" style="margin-right:10px">
                    <div class="caption pull-left">
                        <h4>      
                        <p>{this.opts.data.name}</p>
                        </h4>
                        <small>{this.opts.data.first_name} {this.opts.data.last_name}</small>
                    </div>
                    </div>
                </li>
                <delete-user data="{this.opts.data}" delete_user="{delete_user}"></delete-user>
            </ul>
        </div>
        <script>
            import {globals} from "~/util/util"
            var vm = this;
            var token = sessionStorage.getItem("token")
            
            this.on("mount", function(){
                vm.url = globals.BASE_API + "/user/profile-img/" + this.opts.data.id + "?access_token=" + token; 
            })
            
            this.update_user = function(){
                $("#" + this.opts.data.id).modal('show');
            }
        </script>
</user-admin>
