<user-layout>
    <div class="container-fluid">
        <h1>Users</h1>
        <div class="line"></div>
        <div class="users-container row">
            <filter-users search="{search}" clear_search="{clear_search}"/>
            <user-list data="{this.state}" add_user="{add_user}" load-departments="{load_departments}"
                delete_user="{delete_user}">
            </user-list>
            <nav>
                <ul class="pager">
                    <li onclick="{prev_page}" class="previous disabled"><a ><span aria-hidden="true">&larr;</span> Previous</a></li>
                    <li onclick="{next_page}" class="next disabled"><a >Next <span aria-hidden="true">&rarr;</span></a></li>
                </ul>
            </nav>
        </div>
    </div>
    <script>
        import * as actions from "./userActions"
        import {globals} from "~/util/util"

        var update = this.update
        var ok = 0
        var vm = this;
        vm.fileterUsers = -1;
           
        this.clear_search = function(){
            vm.state.users.data = vm.fileterUsers;
            vm.update();
        }
        
        this.search = function(term){
            if(vm.fileterUsers == -1){
                vm.fileterUsers = [];
            } else {
                vm.state.users.data = vm.fileterUsers;
            }
            var array = vm.state.users.data;
            var newArr = [];
            vm.fileterUsers = array;
            
            for(var i=0; i<= array.length - 1; i++){
                if(array[i].name.toLowerCase().indexOf(term.toLowerCase()) > -1){
                    newArr.push(array[i]);
                }
            }    
            
            vm.state.users.data = newArr;
            vm.update();
        }
        
        this.add_user = function (user) {
            vm.opts.store.dispatch(actions.add_user(user))
        }
        
        this.delete_user = function (user_id) {
            vm.opts.store.dispatch(actions.delete_user(user_id))
        }
        
        this.load_departments = function(faculty){
            vm.opts.store.dispatch(actions.load_departments(faculty));
        }
        
        this.on("update", function () {
            if (this.opts.store && ok == 0) {
                this.opts.store.subscribe(function () {
                    update()
                }).bind(this)
            }
            if (this.opts.store) {
                ok = 1
                this.state = this.opts.store.getState().user
                console.log(this.state)
            }
            
            $(".pager").hide();
            if(vm.state){
                if(vm.state.users){
                    if(vm.state.users.data.length == 0) {
                        $(".pager").hide();
                        return;
                    }
                    $(".pager").show();
                    if(vm.state.users.next_page_url != null){
                      $(".next").removeClass("disabled");
                    }else{
                      $(".next").addClass("disabled");
                    }
                
                    if(vm.state.users.prev_page_url != null){
                        $(".previous").removeClass("disabled");
                    }else{
                        $(".previous").addClass("disabled");
                    }
                }
            }
        })
        
        this.next_page = function(event){
            if(vm.state){
                if(vm.state.users.next_page_url != null){
                    $(event.target).removeClass("disabled")
                    vm.opts.store.dispatch(actions.load_users(vm.state.users.next_page_url + "&client_id="+globals.client_id ));
                    vm.state = vm.opts.store.getState().user; 
                }else{
                    $(event.target).addClass("disabled")
                }
            }
        }
        
        this.prev_page = function(event){
            if(vm.state){
                if(vm.state.users.prev_page_url != null){
                    $(event.target).removeClass("disabled")
                    vm.opts.store.dispatch(actions.load_users(vm.state.users.prev_page_url + "&client_id="+globals.client_id ));
                    vm.state = vm.opts.store.getState().user;
                }else{
                    $(event.target).addClass("disabled")
                }
            }
        }
        
    </script>
</user-layout>