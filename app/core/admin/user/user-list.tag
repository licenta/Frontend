<user-list>
    <div class=" col-md-4 col-md-offset-0 col-sm-3 col-xs-12  col-lg-4">
        <div class="span12">
            <ul class="thumbnails">
                <li class="span5 clearfix">
                    <div class="thumbnail clearfix add" onclick="{open_modal}">
                        <img src="build/img/add-user.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px'>
                        <div class="caption pull-left">
                            <h4>      
                            <p>Add User</p>
                            </h4>
                        </div>
                    </div>
                    <add-user faculty="{this.opts.data.faculty}" departments={this.opts.data.departments} 
                            roles="{this.opts.data.roles}" add-user="{add_user}" load-departments="{load_departments}">
                    </add-user>
                </li>
            </ul>
        </div>
    </div>
    <div class=" col-md-4 col-md-offset-0 col-sm-3 col-xs-12  col-lg-4" each="{this.opts.data.users.data}"  data="{ this }" >
        <user-admin data={this} delete_user="{delete_user}">
        </user-admin>
    </div>
    <script>
        var vm = this;
        
        vm.open_modal = function(){
            
            $("#addUserModal").modal('show');
        }
    </script>
</user-list>