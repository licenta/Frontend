import {globals, $http} from '~/util/util'

const user_error = (message)=> {
    return {
        type: 'SHOW_ERROR',
        data: message
    }
}
const load_users = (url)=> {
	return (dispatch, getState)=> {
		dispatch(users_loading(true))
		$http.get(url)
			.then(function (response_text) {
				var response = JSON.parse(response_text)
				dispatch(users_loaded(response.users))
			}).catch((error)=> {
			console.log(error) 
		}) 
	} 
} 
const add_user = (data)=> { 
	return (dispatch, getState)=> { 
		$http.post(globals.BASE_API + '/user', data) 
			.then(function (response) { 
				console.log(response)
				dispatch(user_added(JSON.parse(response).user)); 
			}).catch((error)=> { 
			var message = JSON.parse(error.responseText).error.data 
			dispatch(user_error(message)) 
			console.log(error); 
		}) 
	} 
}

const delete_user = (data)=> { 
	return (dispatch, getState)=> { 
		$http.delete(globals.BASE_API + '/user/' + data) 
			.then(function (response) { 
				dispatch(user_deleted({id: data})) 
			}).catch((error)=> { 
			var message = JSON.parse(error.responseText).error.name 
			dispatch(user_error(message)) 
			console.log(error); 
		}) 
	} 
}

const load_faculties = ()=> {
    return (dispatch, getState)=> {
       $http.get(globals.BASE_API + "/faculty?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(faculties_loaded(JSON.parse(response).faculty.data))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const load_departments = (faculty)=> {
    return (dispatch, getState)=> {

        $http.get(globals.BASE_API + "/faculty/" + faculty + "/department?with=language&client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(departments_loaded(JSON.parse(response).department.data));
            }).catch((error)=> {
            console.log(error);
        })
    }
}

const load_roles = ()=> {
    return (dispatch, getState)=> {

        $http.get(globals.BASE_API + "/role?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(roles_loaded(JSON.parse(response).role));
            }).catch((error)=> {
            console.log(error);
        })
    }
}

const roles_loaded = (roles)=>{
	return{
		type: "ROLES_LOADED",
		data: roles
	}
}

const faculties_loaded = (faculty)=> {
    return {
        type: "FACULTIES_LOADED",
        data: faculty
    }
}

const user_deleted = (user)=> {
	return {
		type: 'USER_DELETED',
		data: user 
	}
}
const user_added = (user)=> {
	return {
		type: 'USER_ADDED',
		data: user 
	}
}

const users_loaded = (users)=> {
	return {
		type: 'USERS_LOADED',
		data: users 
	}
}
const users_load_more = (current_page)=> {
	return {
		type: 'USER_LOAD_MORE',
		data: current_page 
	}
}
const users_loading = (loading)=> {
	return {
		type: 'USER_LOADING',
		data: loading 
	}
}
const departments_loaded = (departments)=> {
    return {
        type: "DEPARTMENTS_LOADED",
        data: departments
    }
}

export {load_users, add_user, delete_user, load_faculties, load_departments, load_roles}