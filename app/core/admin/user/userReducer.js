const userReducer = (state={message:""},action={})=>{
	switch (action.type) {
		case 'USERS_LOADED':
			return Object.assign({}, state, {users: action.data});
		case 'USER_LOADING':
			return Object.assign({}, state, {isLoading: action.data})
		case 'USER_LOAD_MORE' :
			return Object.assign({}, state, {current_page: action.data})
		case 'USER_ADDED' :
			if(state.users.data.length < 15){
				state.users.data = state.users.data.concat(action.data);
				return Object.assign({}, state, {users: state.users})
			}
		case 'USER_DELETED' :
			var userIndex = state.users.data.findIndex(function (user) {
				return user.id == action.data.id
			})
			console.log(userIndex)
			var obj1 = state.users.data.slice(0, userIndex);
            var obj2 = state.users.data.slice(userIndex + 1);
            state.users.data = obj1.concat(obj2);
            return Object.assign({},state,{sections: state.users})
			
		case 'DEPARTMENTS_LOADED':
			return Object.assign({},state,{departments:action.data});
		case 'FACULTIES_LOADED':
            	return Object.assign({},state,{faculty:action.data});
		case 'ROLES_LOADED':
			return Object.assign({},state,{roles:action.data});
		case 'SHOW_ERROR' :
			return Object.assign({}, state, {message: action.data})
		default:
			return state
		}
	}
export default userReducer