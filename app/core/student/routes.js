import {load_states,stateRouter} from "~/router"
import {reducerRegistry} from "~/reduxStore"

import "./student-sidebar.tag"

//here we import the routes
import state_subject from "./subject/routes"

//here we import the reducers

init()
function init() {
    //here we load the our reducer at runtime
    reducerRegistry.register([
    ])

    //here you add the routes for the admin
    load_states([
        ...state_subject
    ])
    //after we load the states we evalute the current layout
    stateRouter.evaluateCurrentRoute("layout")
}
