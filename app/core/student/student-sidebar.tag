<student-sidebar>
    <li>
        <a class="menu-item" href="#/grades">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Grades</span>
        </a>
    <li>
        <a class="menu-item" href="#/subjects/student">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Subjects</span>
        </a>
    </li>
    <script>
        this.on("update", function () {
            var items = $('.menu-item');
            $.each(items, function (index, value) {
                if ($(value).attr("href") == window.location.hash) {
                    $(value).addClass("selected");
                }
            })

            $('.menu-item').click(function () {
                $(".menu-item").removeClass("selected");
                $(this).addClass("selected");
            })
        })
    </script>
</student-sidebar>