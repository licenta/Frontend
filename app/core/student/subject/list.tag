<subject-list>
    <ul class="list-container row">
        <li class="col-xs-6 col-md-3 col-lg-2" each="{subject in this.opts.subjects}">
            <yield />
        </li>
    </ul>
</subject-list>