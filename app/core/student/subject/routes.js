import {reduxStore} from "~/reduxStore"

import "./container.tag"
import "./list.tag"
import "./subject.tag"
import {load_subjects} from "~/common/subject/actions"

const states = [{
    name: 'layout.subject.student',
    route: '/student',
    template: "subject-container",
    activate: (context)=> {
        var tag = context.domApi
        tag.opts.store = reduxStore
        tag.opts.state = reduxStore.getState().subject
        // tag.opts.state = {}
        // tag.opts.state.subjects = [{name:"Chris"},{name:"Bety"},{name:"Carolina"}]
        tag.opts.store.dispatch(load_subjects("/user/subject?user_id=9"))
        tag.update()
        // setTimeout(function () {
        //     tag.opts.state.subjects = [{name:"Chris"}]
        //     tag.update()
        // },4000)
        context.on('destroy', function () {
            tag.unmount()
        })
    }
}]

export default states