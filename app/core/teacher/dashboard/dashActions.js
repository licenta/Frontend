import {globals, $http} from "~/util/util"
const load_user_details = (url)=> {
	return (dispatch, getState)=> {
		$http.get(url)
			.then(function (response_text) {
				var response = JSON.parse(response_text)
				dispatch(detail_user(response))
			}).catch((error)=> {
			console.log(error) 
		}) 
	} 
}

const detail_user = (user)=>{
	return{
		type: "USER_DETAILS",
		data: user
	}
}

const load_academic_years = (url)=> {
	return (dispatch, getState)=> {
		$http.get(globals.BASE_API + url +  "&client_id=" + globals.client_id)
			.then(function (response_text) {
				var response = JSON.parse(response_text)
				dispatch(years_loaded(response.academic_year.data))
			}).catch((error)=> {
			console.log(error) 
		}) 
	} 
}

const years_loaded = (years)=>{
	return{
		type: "YEARS_LOADED",
		data: years
	}
}

const load_subjects = (url)=> {
    return (dispatch, getState)=> {
        $http.get(url)
            .then(function (response) {
                dispatch(subjects_loaded(JSON.parse(response).subject))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const subjects_loaded = (subjects)=> {
    return {
        type: "SUBJECTS_LOADED",
        data: subjects
    }
}

const load_activity_types = () => {
    return (dispatch, getState)=> {
        $http.get(globals.BASE_API +"/activity-type?client_id=" + globals.client_id)
            .then(function (response) {
                dispatch(activity_types_loaded(JSON.parse(response)))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const activity_types_loaded = (activity_types) => {
    return {
        type: "ACTIVITY_TYPES_LOADED",
        data: activity_types
    }
}



const load_grades  = (url)=> {
    return (dispatch, getState)=> {
        $http.get(url)
            .then(function (response) {
                dispatch(grades_loaded(JSON.parse(response).grade.data))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const grades_loaded = (grades) =>{
    return {
        type: "GRADES_LOADED",
        data: grades
    }
}

const load_schedule  = (url)=> {
    return (dispatch, getState)=> {
        $http.get(url)
            .then(function (response) {
                dispatch(schedule_loaded(JSON.parse(response).data))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const schedule_loaded = (schedule) =>{
    return {
        type: "SCHEDULE_LOADED",
        data: schedule
    }
}


export {load_user_details, load_subjects, load_academic_years, load_activity_types,  load_grades, load_schedule}