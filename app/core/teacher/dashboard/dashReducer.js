const dashReducer = (state={message:""},action={})=>{
	switch (action.type) {
	    case "USER_DETAILS":
            return  Object.assign({}, state, {user: action.data});
        case "SUBJECTS_LOADED":
              if(state.subjects !== undefined){
                   return Object.assign({}, state, {
                    subjects: [
                        ...state.subjects,
                        ...action.data
                    ]
                })
              }else{
                 return Object.assign({}, state, {subjects: action.data});
              }
        case "YEARS_LOADED":
            return  Object.assign({}, state, {years: action.data});
        case "ACTIVITY_TYPES_LOADED":
            return Object.assign({},state,{activity_types:action.data});
        case "GRADES_LOADED":
             console.log("g")
             return  Object.assign({}, state, {grades: action.data});
        case "SCHEDULE_LOADED":
        console.log("g")
             return Object.assign({}, state, {schedule: action.data});
        default:
			return state
		}
	}
export default dashReducer