<dashboard-layout>
    <div class="container-fluid">
        <div class="dashboard-container">
            <div id="first-dashboard-container">
                <grades-dashboard grades="{state.grades}"></grades-dashboard>
            </div>
            <div id="second-dashboard-container">
                <subjects-dashboard subjects="{state.subjects}"
                                    activity_types="{state.activity_types.activity_type}" />
            </div>
            <div id="third-dashboard-container">
                <schedule-dashboard/>
            </div>
        </div>
    </div>
    <script>
        import { load_academic_years, load_subjects, load_grades, load_schedule} from "./dashActions"
        import {globals} from "~/util/util"

        var update = this.update;
        var ok = 0;
        var vm = this;

        this.on("mount", function(){
            ok = 0;
        })

        this.on("update", function () {
            if (this.opts.store && ok == 0) {
                this.opts.store.subscribe(function () {
                    update()
                }).bind(this)
            }
            if (this.opts.store) {
                this.state = this.opts.store.getState().dash
                console.log(this.state)
                if(this.state.user && this.state.years && ok == 0){
                    load_subject();
                    load_schedules();
                    ok = 1;
                }

            }
        });

        function load_subject(){
            var user_id = vm.state.user.id;
            for(var i = 0; i < vm.state.years[0].semester.length ;i++){
                var sem = vm.state.years[0].semester[i].id
                vm.opts.store.dispatch(load_subjects( globals.BASE_API + "/user/subject?=user_id=" + user_id +
                        "&semester_id=" + sem + "&with=SubjectActivityType"))
            }
        }
        
        function load_schedules(){
            var user_id = vm.state.user.id;
            var parity = 1;
            var date = new Date();
            var month = date.getMonth();
            if( 3 <= month < 5){
                parity = 2;
            }
              
            var sem = vm.state.years[0].semester[parity-1].id
            vm.opts.store.dispatch(load_schedule(globals.BASE_API + "/user/subject-group?user_id=" + user_id +"?semester_id=" + sem
             + "&with=Room"))
        }
        
        this.on("unmount", function () {
            this.state.subjects = undefined;
            this.state = null;
        })
    </script>
</dashboard-layout>
