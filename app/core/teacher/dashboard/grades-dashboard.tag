<grades-dashboard>
	<div id="grades-dashboard">
		<div class="title-link"><a href="/#/grades">Grades</a></div>
		<students-dash-list grades="{this.opts.grades}"></students-dash-list>
	</div>
</grades-dashboard>