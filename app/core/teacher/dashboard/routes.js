import "./dashboard-layout.tag"
import "./grades-dashboard.tag"
import "./schedule-dashboard.tag"
import "./subjects-dashboard.tag"
import "./subject-dash-list.tag"
import "./subject-dash.tag"
import "./students-dash-list.tag"
import "./student-dash.tag"

import {reduxStore} from "~/reduxStore"
import { load_academic_years, load_activity_types, load_user_details, load_students, load_grades, load_schedule} from "./dashActions"
import {globals} from "~/util/util"
const states = [{
    name: 'layout.dashboard',
    route: '/dash',
    template: 'dashboard-layout',
    activate:function(context) {
        var tag = context.domApi
        tag.opts.store = reduxStore
        var user_id = JSON.parse(sessionStorage.getItem("userdata")).id;
        console.log(tag.opts.store)
        //load current subjects
        //get current academic_year
        var year;
        var date = new Date();
        var month = date.getMonth();
        if(month < 8){
             year = date.getFullYear()-1;
        }else{
             year = date.getFullYear();
        }
      
        
        tag.opts.store.dispatch(load_user_details(globals.BASE_API + "/me?with=Entity"));
        tag.opts.store.dispatch(load_activity_types());
        tag.opts.store.dispatch(load_academic_years("/academic-year?start_year=" + year + "&with=Semester"));
        //tag.opts.store.dispatch(load_students( globals.BASE_API + "/user?entity_type=student"));
        tag.opts.store.dispatch(load_grades(globals.BASE_API + "/evaluation/10/grade?professor_user_id=" + user_id +"sort[]=created_at,desc&with=Student"));
    
      
    }
}]

export default states