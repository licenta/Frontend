<schedule-dashboard>
	<div id="schedule-dashboard">
		<div class="title-link"><a href="/#/schedule">Schedule</a></div>
		<table id="schedule-table-header">
			<thead>
			<th>Hour</th>
			<th>Monday</th>
			<th>Tuesday</th>
			<th>Wednesday</th>
			<th>Thursday</th>
			<th>Friday</th>
			</thead>
		</table>
		<div id="schedule-table-container">
			<table id="schedule-table">

				<tbody>
				<tr>
					<td>08</td>
					<td rowspan="2"><div class="bubble double course" title="II/1">II/1</div></td>
					<td rowspan="2"><div class="bubble double course" title="II/1">II/1</div></td>
					<td rowspan="2"><div class="bubble double seminar" title="L220">L220</div></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>09</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>10</td>
					<td rowspan="2"><div class="bubble double course" title="II/1">II/1</div></td>
					<td rowspan="2"><div class="bubble double course" title="II/1">II/1</div></td>
					<td rowspan="2"><div class="bubble double seminar" title="L225">L225</div></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>11</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>12</td>
					<td rowspan="2"><div class="bubble double course" title="II/1">II/1</div></td>
					<td></td>
					<td rowspan="2"><div class="bubble double laboratory" title="L320">L320</div></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>13</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>14</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td rowspan="2"><div class="bubble double seminar" title="A130">A130</div></td>
				</tr>
				<tr>
					<td>15</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>16</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td rowspan="2"><div class="bubble double seminar" title="A130">A130</div></td>
				</tr>
				<tr>
					<td>17</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>18</td>
					<td></td>
					<td></td>
					<td></td>
					<td rowspan="2"><div class="bubble double laboratory" title="L320">L320</div></td>
					<td></td>
				</tr>
				<tr>
					<td>19</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>20</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>21</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				</tbody>
			</table>
		</div>

		<div class="legend">
			<div class="group"><div class="box course"></div>course</div>
			<div class="group"><div class="box seminar"></div>seminar</div>
			<div class="group"><div class="box laboratory"></div>laboratory</div>
		</div>
	</div>
	<script>
		this.on('mount', function () {
			$('.bubble').click(function() {
				location.href = "/#/schedule";
			})
		})
	</script>
</schedule-dashboard>
