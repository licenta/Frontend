<student-dash>
    <div>
        <div class="avatar">
            <img alt="" class="img-circle" src="{url}">
        </div>
        <div class="info" >
            <div class="title">
                <a target="_blank" >{this.opts.data.student.name}</a>
            </div>
            <div class="desc">{this.opts.data.student.first_name} {this.opts.data.student.last_name}</div>
        </div>
        <div class="grade-details">
            <h4 class="alert alert-success"> Added grade  <span class="badge">{this.opts.data.grade}</span> 
                on  <span class="badge">{this.opts.data.created_at}</span></h3>
        </div>
    </div>
    <script>
        import {globals} from "~/util/util"
        var token = sessionStorage.getItem("token")
       // this.id = this.opts.data.id;
        this.url = globals.BASE_API  + "/user/profile-img/" + this.opts.data.student.id + "?access_token=" + token;

    </script>
</student-dash>