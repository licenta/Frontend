<subject-dash>
    <div class="tile">
        <div class="department">
            <p if="{displayed_activity.length != 0}"><span class="label label-default activity-type-span" onclick="{add_activity_type}">{displayed_activity}</span></p>
            <p if="{displayed_activity.length == 0}" ><span  class="label label-default activity-type-span" onclick="{add_activity_type}"
                >Add Activity type</span></p>
        </div>
        <div class="line"></div>
        <div class="title">
            <p>{this.opts.name}</p>
        </div>
        <div class="details" style="background-color: #EA6D4E">
            <p></p>
        </div>
    </div>
    <script>
        import {globals, $http} from "~/util/util"
        var vm = this;
        vm.displayed_activity = "";
        vm.available_activities = [];
        vm.current_activities = [];
        
        
        
        this.on("update", function(){
             vm.displayed_activity = "";
             vm.available_activities = [];
             vm.current_activities = [];
             
             
             $.each(vm.parent.opts.activity_types, function( index, value) {
                 var id = existsActivity(value.id)
                    if(id != -1){
                        vm.displayed_activity += value.name;
                        vm.displayed_activity += " ";
                        var obj = value;
                        $.extend(obj, {"subject_act_id": id});
                        vm.current_activities.push(obj);
                    }else{
                        vm.available_activities.push(value);
                    }
             });
        });
        
        function existsActivity(activity_id){
            if(vm.opts.subject_activity_type != undefined){
                for(var i=0; i<=vm.opts.subject_activity_type.length - 1;i++){
                    if(vm.opts.subject_activity_type[i].activity_type_id == activity_id){
                        return vm.opts.subject_activity_type[i].id;
                    }
                } 
            }
           
            
            return -1;
        }  
    </script>
</subject-dash>