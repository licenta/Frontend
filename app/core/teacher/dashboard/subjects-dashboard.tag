<subjects-dashboard>
	<div id="subjects-dashboard">
		<div class="title-link"><a href="/#/subjects/teacher">Subjects</a></div>
		<subject-dash-list subjects="{this.opts.subjects}" 
                 activity_types="{this.opts.activity_types}" ></subject-dash-list>
	</div>
	<script>
	</script>
</subjects-dashboard>