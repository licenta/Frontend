<add-grade>
    <div class="modal fade" id="addGrade{this.opts.id}" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Add Grade</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-md-12 col-xs-12">
                        <label>Feedback:</label>
                        <p><textarea name="feedback" rows="4" cols="50" style="border-radius:4px;width:100%"></textarea></p>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <label>Grade: <input type="text" id="grade{this.opts.id}" style="border:0;font-weight:bold;"></label>
                        <p><div class="slider"></div></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button" onclick="{evaluate_stud}"
                                 data-dismiss="modal"><span class="glyphicon glyphicon-check"></span> Save Changes
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <script>
        var vm = this;
        
        this.on('updated', function() {
            var $modal = $('#addGrade' + vm.opts.id);
            var el =  $modal.find( "#grade" + vm.opts.id);
          $modal.find('.slider').slider({
              animate: 'slow',
              min: 1,
              max: 10,
              value:10,
              range: 'min',
              slide: function( event, ui ) {
                  console.log(ui.value)
                  el.val(ui.value);
               }
          }); 
          el.val( $modal.find('.slider').slider( "value" ) );
        })

        $(function(){
              var el =  $( "#grade" + vm.opts.id);
          $('.slider').slider({
              animate: 'slow',
              min: 1,
              max: 10,
              value:10,
              slide: function( event, ui ) {
                  console.log(ui.value)
                  el.val(ui.value);
               }
          }); 
          el.val( $( ".slider" ).slider( "value" ) );
        })
        
        vm.evaluate_stud = function(){
            var data = {
                "grade": $( "#grade" + vm.opts.id).val(),
                "feedback": vm.feedback.value,
                "user_id": vm.opts.id,
                "evaluation_id": 10,
            }
            console.log(data);
            vm.parent.parent.opts.evaluate(data);
        }
    </script>  
</add-grade>