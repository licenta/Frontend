<grades-layout>
     <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <h1>Grades</h1>
                <div class="line"></div>
            </div>
        </div>
        <div class="grades-container">
            <filter-grades  select_semester="{select_semester}"
                            years="{state.years}"
                            subjects="{state.subjects}"
                            load_students = "{load_student}"></filter-grades>
           <students-list students="{state.students}" subjects="{state.subjects}" evaluate="{evaluate}"></students-list>
        </div>
    </div>
    <script>
        import { load_academic_years, load_subjects, load_students, add_grade} from "./gradesActions"
        import {globals} from "~/util/util"
        var update = this.update;
        var ok = 0;
        var vm = this;
        var store = this.opts.store
        
        this.on("update", function () {
            if (this.opts.store && ok == 0) {
                this.opts.store.subscribe(function () {
                    update()
                }).bind(this)
            }
            if (this.opts.store) {
                this.state = this.opts.store.getState().grades;
                if(this.state.user && ok == 0){
                    load_years();
                    ok = 1;
                } 
            }
        });
        
          
        function load_years(){
            vm.opts.store.dispatch(load_academic_years("/academic-year?with=Semester"))
        }
        
        this.select_semester = function(year, semester){
            var user_id = vm.state.user.id;
            if(vm.state.students){
                vm.state.students = undefined;
                vm.update();
            }
            
            if(year != -1){
                if(semester != -1){
                    vm.opts.store.dispatch(load_subjects( globals.BASE_API + "/user/subject?=user_id=" + user_id +  
                         "&semester_id=" + semester + "&with=SubjectActivityType"))
                }else{
                    vm.state.subjects = [];
                    vm.state.students = undefined;
                    vm.update();
                }
            }
            
        }
        
        this.load_student = function(subject_id){
             if(subject_id != -1){
               vm.opts.store.dispatch(load_students( globals.BASE_API + "/subject/" + subject_id + "/user"));
             }else{
                  vm.state.students = undefined;
                  vm.update();
             }
             
        }
        
        this.evaluate = function(data){
            vm.opts.store.dispatch(add_grade(data));
        }
        
        this.on("unmount", function ()
         {
             vm.state.students = undefined;
            this.state = null;
        })
    </script>
</grades-layout>