import {globals, $http} from "~/util/util"
const load_user_details = (url)=> {
	return (dispatch, getState)=> {
		$http.get(url)
			.then(function (response_text) {
				var response = JSON.parse(response_text)
				dispatch(detail_user(response))
			}).catch((error)=> {
			console.log(error) 
		}) 
	} 
}

const detail_user = (user)=>{
	return{
		type: "USER_DETAILS_GRADES",
		data: user
	}
}

const load_academic_years = (url)=> {
	return (dispatch, getState)=> {
		dispatch(years_loaded(true))
		$http.get(globals.BASE_API + url +  "&client_id=" + globals.client_id)
			.then(function (response_text) {
				var response = JSON.parse(response_text)
				dispatch(years_loaded(response.academic_year.data))
			}).catch((error)=> {
			console.log(error) 
		}) 
	} 
}

const years_loaded = (years)=>{
	return{
		type: "YEARS_LOADED_GRADES",
		data: years
	}
}

const load_subjects = (url)=> {
    return (dispatch, getState)=> {
        $http.get(url)
            .then(function (response) {
                dispatch(subjects_loaded(JSON.parse(response).subject))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const subjects_loaded = (subjects)=> {
    return {
        type: "SUBJECTS_LOADED_GRADES",
        data: subjects
    }
}

const load_students  = (url)=> {
    return (dispatch, getState)=> {
        $http.get(url)
            .then(function (response) {
                dispatch(students_loaded(JSON.parse(response).users))
            }).catch((error)=> {
            console.log(error)
        })
    }
}

const students_loaded = (students) =>{
    return {
        type: "STUDENTS_LOADED_GRADES",
        data: students
    }
}

const add_grade = (data) =>{
	return (dispatch, getState)=> {
        $http.post(globals.BASE_API + "/grade",data)
            .then(function (response) {
                dispatch(grade_added(JSON.parse(response).grade));
            }).catch((error)=> {
            var message = JSON.parse(error.responseText).error.name
            console.log(error);
        })
    }
}

const grade_added = (grade)=> {
    return {
        type: "GRADE_ADDED",
        data: grade
    }
}
export {load_user_details, load_subjects, load_academic_years, load_students, add_grade}