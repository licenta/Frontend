const gradesReducer = (state={message:""},action={})=>{
	switch (action.type) {
	    case "USER_DETAILS_GRADES":
            return  Object.assign({}, state, {user: action.data});
        case "SUBJECTS_LOADED_GRADES":
             return  Object.assign({}, state, {subjects: action.data});
        case "YEARS_LOADED_GRADES":
            return  Object.assign({}, state, {years: action.data});
        case "STUDENTS_LOADED_GRADES":
            return  Object.assign({}, state, {students: action.data});
        case "GRADE_ADDED":
            return state
		default:
			return state
		}
	}
export default gradesReducer