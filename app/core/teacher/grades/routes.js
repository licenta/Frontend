import "./grades-layout.tag"
import "./students-list.tag"
import "./student-layout.tag"
import "./filter-grades.tag"
import "./add-grade.tag"

import {reduxStore} from "~/reduxStore"
import {load_user_details} from "./gradesActions"
import {globals} from "~/util/util"
const states = [{
    name: 'layout.grades',
    route: '/grades',
    template: 'grades-layout',
    activate:function(context) {
        var tag = context.domApi
        tag.opts.store = reduxStore
         console.log(tag.opts.store)
        tag.opts.store.dispatch(load_user_details(globals.BASE_API + "/me?with=Entity"));
    }
}]

export default states