<student-layout>
    <div onclick={open_modal}>
        <div class="avatar">
            <img alt="" class="img-circle" src="{url}">
        </div>
        <div class="info" >
            <div class="title">
                <a target="_blank" >{this.opts.data.name}</a>
            </div>
            <div class="desc">{this.opts.data.first_name} {this.opts.data.last_name}</div>
        </div>
    </div>
    <add-grade id="{this.opts.data.id}" evaluate = "{this.opts.evaluate}"></add-grade>
    <script>
        import {globals} from "~/util/util"
        var token = sessionStorage.getItem("token")
        this.id = this.opts.data.id;
        this.url = globals.BASE_API  + "/user/profile-img/" + this.id + "?access_token=" + token;
        
        this.open_modal = function(){
            $("#addGrade" + this.opts.data.id).modal('show');
        }
    </script>   
</student-layout>