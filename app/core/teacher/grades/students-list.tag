<students-list>
    <div class="row">
        <h3 class="alert alert-danger no-filters-message" if="{filterNotOk}" >Filter options not ok!</h3>
        <h3 class="alert alert-warning no-filters-message" if={!filterNotOk && subjectsNotFount}>No data for this search !</h3>

        <div if="{filterNotOk == false}" class="col-xs-12 col-sm-6 col-md-3 student-container" each="{this.opts.students}">
            <student-layout data="{this}" evaluate = "{this.opts.evaluate}" >
            </student-layout>
        </div>
     </div>
    <script>
        this.filterNotOk = true;
        this.subjectsNotFount = true;
        
        this.on("update", function(){
            this.filterNotOk = true;
            if(this.opts.students){
                this.filterNotOk = false;
                if(this.opts.students.length != 0) {
                    this.subjectsNotFount = false;
                }
                else {
                    this.subjectsNotFount = true;
                }
            }
        });
    </script>    
</students-list>