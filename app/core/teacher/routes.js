import {load_states,stateRouter} from "~/router"
import {reducerRegistry, reduxStore} from "~/reduxStore"

// //here we import the routes
import states_dashboard from "./dashboard/routes"
import states_grades from "./grades/routes"
import states_schedule from "./schedule/routes"
import states_subject from "./subjects/routes"

import "./teacher-sidebar.tag"

import {load_user_details} from "./teacherActions"

//here we import the reducers
import subject from "./subjects/subjectReducer"
import teacher from "./teacherReducer"
import grades from "./grades/gradesReducer"
import dash from "./dashboard/dashReducer"
import {globals} from "~/util/util"

init()
function init() {
    //here we load the our reducer at runtime
    reducerRegistry.register([
        {"teacher": teacher},
        {"subject": subject},
        {"grades":grades},
        {"dash":dash}
    ])

    //here you add the routes for the admin
    load_states([
        ...states_subject,
        ...states_dashboard,
        ...states_grades,
        ...states_schedule
    ])
    //after we load the states we evalute the current layout
    stateRouter.evaluateCurrentRoute("layout")
    
}
