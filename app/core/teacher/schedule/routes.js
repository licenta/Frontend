import "./schedule-layout.tag"

import {reduxStore} from "~/reduxStore"

const states = [{
    name: 'layout.schedule',
    route: '/schedule',
    template: 'schedule-layout',
    activate:function(context) {
        var tag = context.domApi
        tag.opts.store = reduxStore
    }
}]

export default states