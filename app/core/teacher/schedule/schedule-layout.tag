<schedule-layout>
     <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <h1>Schedule</h1>
                <div class="line"></div>
            </div>
        </div>
        <div class="schedule-container">
            <div id="calendar"></div>
        </div>
    </div>
    <script>
       this.on("mount", function(){
            var height = $('.container').innerHeight() - 120;
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'agendaWeek,agendaDay'
                },
                editable: false,
                 events: [
                    {
                        title  : 'Subject: Algoritmica grafelor\nActivity: course\nRoom: II/1',
                        color  : '#16A6C7',
                        start  : '08:00',
                        end    : '10:00',
                        dow    : [1],
                    },
                    {
                        title  : 'Subject: Sisteme de operare\nActivity: course\nRoom: II/1',
                        color  : '#16A6C7',
                        start  : '10:00',
                        end    : '12:00',
                        dow    : [1],
                    },
                    {
                        title  : 'Subject: Structuri de date si algoritmi\nActivity: course\nRoom: II/1',
                        color  : '#16A6C7',
                        start  : '12:00',
                        end    : '14:00',
                        dow    : [1],
                    },
                    {
                        title  : 'Subject: Algoritmica grafelor\nActivity: course\nRoom: II/1',

                        color  : '#16A6C7',
                        start  : '08:00',
                        end    : '10:00',
                        dow    : [2],
                    },
                    {
                        title  : 'Subject: Geometrie\nActivity: course\nRoom: II/1',
                        color  : '#16A6C7',
                        start  : '10:00',
                        end    : '12:00',
                        dow    : [2],
                    },
                    {
                        title  : 'Subject: Sisteme de operare\nActivity: seminar\nRoom: L220',
                        color  : '#39C716',
                        start  : '08:00',
                        end    : '10:00',
                        dow    : [3],
                    },
                    {
                        title  : 'Subject: Metode avansate de rezolvare a problemelor de matematica si infromatica\nActivity: seminar\nRoom: L225',
                        color  : '#39C716',
                        start  : '10:00',
                        end    : '12:00',
                        dow    : [3],
                    },
                    {
                        title  : 'Subject: Algoritmica grafelor\nActivity: laboratory\nRoom: L320',
                        color  : '#16c79e',
                        start  : '12:00',
                        end    : '14:00',
                        dow    : [3],
                    },
                    {
                        title  : 'Subject: Sisteme de operare\nActivity: laboratory\nRoom: L320',
                        color  : '#16c79e',
                        start  : '18:00',
                        end    : '20:00',
                        dow    : [4],
                    },
                    {
                        title  : 'Subject: Algoritmica grafelor\nActivity: seminar\nRoom: A130',
                        color  : '#39C716',
                        start  : '14:00',
                        end    : '16:00',
                        dow    : [5],
                    },
                    {
                        title  : 'Subject: Algoritmica grafelor\nActivity: seminar\nRoom: A130',
                        color  : '#39C716',
                        start  : '16:00',
                        end    : '18:00',
                        dow    : [5],
                    },
                ],
                defaultView: 'agendaWeek',
                firstDay: 1,
                weekends: false,
                businessHours: {
                    start: '08:00',
                    end: '22:00',
                    dow: [1,2,3,4,5]
                },
                height: height
            });
       })
    </script>
</schedule-layout>