<filter-subjects>
    <div class="auto-select row">
        <div class="col-xs-12  col-sm-6 col-md-4 select-container">
            <label>Year:</label>
            <select class="selectpicker show-tick" name="year" data-live-search="true" style="display: none;" 
                    onchange="{select_year}" data-style="btn-success">
                <option each="{this.opts.years}" value="{id}">
                    {start_year} - {end_year}
                </option>
            </select>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 select-container">
            <label>Semester:</label>
            <select class="selectpicker show-tick" name="semester" style="display: none;" 
                onchange="{semester_change}" data-style="btn-success">
                <option  value="{-1}">Nothing Selected</option>
                <option each="{semesters}" value="{id}" >
                    Semester - {parity}
                </option>
            </select>
        </div>
    </div>
    
    <script>
        this.semesters = [];
        
        this.on('updated', function() {
            $('.selectpicker').selectpicker('refresh');
        })
        
        this.select_year = function(){
          this.parent.state.subjects = undefined;
          this.parent.update();
          for(var i = 0; i <= this.opts.years.length; i++){
                if(this.opts.years[i].id == this.year.value){
                    this.semesters = this.opts.years[i].semester;
                    break;
                }
            }
        }
        
        this.on("update", function(){
            if(this.opts.years && this.year.value != ""){
                for(var i = 0; i <= this.opts.years.length; i++){
                    if(this.opts.years[i].id == this.year.value){
                        this.semesters = this.opts.years[i].semester;
                        break;
                    }
                } 
            }
           
        })
        
        this.semester_change = function(){
            this.opts.select_semester(this.year.value, this.semester.value);
        }
             
    </script>
</filter-subjects>