import "./subjects-teacher-layout.tag"
import "./filter-subjects.tag"
import "./subject-list.tag"
import "./subject-teacher.tag"

import riot from "riot"
import {reduxStore} from "~/reduxStore"
import {load_user_details, load_activity_types} from "./subjectActions"
import {globals} from "~/util/util"

const states = [{
    name: 'layout.subject.teacher',
    route: '/teacher',
    template: 'subjects-teacher-layout',
    activate: (context)=> {
        var tag = context.domApi;
        tag.opts.store = reduxStore;
         console.log(tag.opts.store)
        tag.opts.store.dispatch(load_user_details(globals.BASE_API + "/me?with=Entity"));
        tag.opts.store.dispatch(load_activity_types());
 
    }
}]

export default states