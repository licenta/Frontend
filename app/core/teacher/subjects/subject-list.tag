<subject-list>
    <div id="list-container">
        <h3 class="alert alert-danger no-filters-message" if={filterNotOk}>Filter options not ok!</h3>
        <h3 class="alert alert-warning no-filters-message" if={!filterNotOk && subjectsNotFount}>No data for this search !</h3>

        <div class="tile-container" each="{this.opts.subjects}">
            <subject-teacher name="{name}" id="{id}" 
                            activity-types="{this.opts.activity_types}" 
                            subject_activity_type="{subject_activity_type}" >
            </subject-teacher>
        </div>
    </div>
    <script>
        this.filterNotOk = true;
        this.subjectsNotFount = true;
        this.disciplines = [];
        this.examinations = [];
        
        this.on("update", function(){
         
            this.filterNotOk = true;
            if(this.opts.subjects){
                this.filterNotOk = false;
                if(this.opts.subjects.length != 0) {
                    this.subjectsNotFount = false;
                }
                else {
                    this.subjectsNotFount = true;
                }
            }
        });

        var tileWidth = 250;
        var tileMinDistance = 20;

        var $style = $('<style>', {type: 'text/css'});
        $("body").append($style);

        var width = -1;

        var resizeFunc = function(){
            var newWidth = $('#list-container').innerWidth();
            if (newWidth == width) {
                return;
            }
            width = newWidth;
            if(!width) {
                setTimeout(resizeFunc, 100);
                return;
            }


            var noOfTiles = parseInt(width / (tileWidth + tileMinDistance));
            var rest = width - noOfTiles * (tileWidth + tileMinDistance);
            var padding = rest / noOfTiles / 2;

            $style.html('.tile-container{padding: 0 ' + padding + 'px;}');
        }

        $(window).resize(resizeFunc);
        setInterval(resizeFunc, 1000);
        resizeFunc();
    </script>
</subject-list>