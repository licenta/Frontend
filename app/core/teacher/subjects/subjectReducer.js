const subjectReducer = (state={message:""},action={})=>{
    console.log("init")
	switch (action.type) {
	    case "USER_DETAILS_SUBJECT":
            return  Object.assign({}, state, {user: action.data});
        case "SUBJECTS_LOADED_SUBJECT":
             return  Object.assign({}, state, {subjects_subj: action.data});
        case "YEARS_LOADED_SUBJECT":
            return  Object.assign({}, state, {years: action.data});
        case "ACTIVITY_TYPES_LOADED_SUBJECT":
            return Object.assign({},state,{activity_types:action.data});
		default:
			return state
		}
	}
export default subjectReducer