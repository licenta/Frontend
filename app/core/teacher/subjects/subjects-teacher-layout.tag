<subjects-teacher-layout>
    <filter-subjects 
                 select_semester="{select_semester}"
                 years="{state.years}"
                 />
    <subject-list subjects="{state.subjects_subj}" 
                 activity-types="{state.activity_types.activity_type}" 
                />
    <script>
        import { load_academic_years, load_subjects} from "./subjectActions"
        import {globals} from "~/util/util"
        
        var update = this.update;
        var ok = 0;
        var vm = this;
        this.state
        this.current_department = null;
        this.current_faculty = null;
        var store = this.opts.store
              
        this.current_year = -1;
        this.current_semester = -1;
    
        
        this.on("mount", function(){
            ok = 0;  
        })
        
        this.on("update", function () {
            if (this.opts.store && ok == 0) {
                this.opts.store.subscribe(function () {
                    update()
                }).bind(this)
            }
            if (this.opts.store) {
                this.state = this.opts.store.getState().subject;
                
                if(this.state.user && ok == 0){
                    this.current_department = this.state.user.entity.department_id;
                    load_years();
                    ok = 1;
                }
                 
            }
        });
        
        function load_years(){
            vm.opts.store.dispatch(load_academic_years("/academic-year?with=Semester"));
        }
        
        this.select_semester = function(year, semester){
            var user_id = vm.state.user.id;
            if(year != -1){
                if(semester != -1){
                    vm.opts.store.dispatch(load_subjects( globals.BASE_API + "/user/subject?=user_id=" + user_id +  
                         "&semester_id=" + semester + "&with=SubjectActivityType"))
                }else{
                    vm.state.subjects_subj = undefined;
                    vm.update();
                }
            }else{
                vm.state.subjects_subj = undefined;
                vm.update();
            }
            
        }
        
        this.on("unmount", function () {
            vm.state.subjects_subj = undefined;
            this.state = null;
        })
  
    </script>
</subjects-teacher-layout>