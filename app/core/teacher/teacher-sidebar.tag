<teacher-sidebar>
    <li>
        <a class="menu-item" href="#/dash">
            <span class="icon-dashboard" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Dashboard</span>
        </a>
    </li>
    <li>
        <a class="menu-item" href="#/subjects/teacher">
            <span class="icon-pencil2" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Subjects</span>
        </a>
    </li>
    <li>
        <a class="menu-item" href="#/grades">
            <span class="icon-files" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Grades</span>
        </a>
    </li>
    <li>
        <a class="menu-item" href="#/schedule">
            <span class="icon-calendar" aria-hidden="true"></span>
            <span class="fa fa-anchor solo">Schedule</span>
        </a>
    </li>
    
    <script>
        this.on("update", function () {
            var items = $('.menu-item');
            $.each(items, function (index, value) {
                if ($(value).attr("href") == window.location.hash) {
                    $(value).addClass("selected");
                }
                else {
                    $(value).removeClass("selected");
                }
            })

            $('.menu-item').click(function () {
                $(".menu-item").removeClass("selected");
                $(this).addClass("selected");
            })
        })
    </script>
</teacher-sidebar>