import {globals, $http} from "~/util/util"
const load_user_details = (url)=> {
	return (dispatch, getState)=> {
		$http.get(url)
			.then(function (response_text) {
				var response = JSON.parse(response_text)
				dispatch(detail_user(response))
			}).catch((error)=> {
			console.log(error) 
		}) 
	} 
}

const detail_user = (user)=>{
	return{
		type: "USER_DETAILS",
		data: user
	}
}


export {load_user_details}