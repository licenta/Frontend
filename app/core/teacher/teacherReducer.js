const teacherReducer = (state={title:"Teacher reducer"},action={})=>{
       switch(action.type){
        case "USER_DETAILS":
            return  Object.assign({}, state, {user: action.data});
        default:
            return state   
       }
}

export default teacherReducer