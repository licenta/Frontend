<header>
    <div class="navbar-container">
        <nav class="navbar">
            <div class="navbar-header">
                <div class="menu-button-expand glyphicon glyphicon-menu-hamburger"></div>
                <a class="navbar-brand" href="#/subjects/{role}"><i class="icon-lamp"></i> <span class="logo-text">Luminous</span></a>
            </div>
            <div>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div>
                            <div id="user-data">
                                <img src="{url}" alt="" class="img-circle">
                                <span>{user_name}</span>
                            </div>
                            <a href="#{role}" data-toggle="modal" aria-label="Left Align">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <span onclick="{logout}" class="pointer logout-link" aria-label="Left Align">
                                Logout
                                <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                            </span>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div id="overlay"></div>
    <user-settings></user-settings>
    <script>
        import {globals, $http} from "~/util/util"
        import {stateRouter} from "~/router"
        import {reduxStore} from "~/reduxStore"
        import {logout} from "~/common/authentification/authService"
        import {getCookie} from "~/util/util"

        this.logout = logout
        this.user_name = "User Name"
        this.role = sessionStorage.getItem("role")
        var vm = this;
        
        this.on('mount', function() {
            vm.user_name = sessionStorage.getItem("name")
            var token = sessionStorage.getItem("token")
            vm.id = JSON.parse(sessionStorage.getItem("userdata")).id
            vm.url = globals.BASE_API  + "/user/profile-img/" + vm.id + "?access_token=" + token;
             
            $(".menu-button-expand").click(function(){
                if($(this).hasClass("active")){
                    $("#sidebar-wrapper").removeAttr("style");
                    $("#overlay").hide();
                    
                    $(this).removeClass("active");
                }
                else {
                    $("#sidebar-wrapper").css({transform: 'translate(0, 0)', opacity: 1});
                    $("#overlay").show();
                    
                    $(this).addClass("active");
                }
            });
            
            $("#overlay").click(function(){
                $("#sidebar-wrapper").removeAttr("style");
                $(this).hide();
                
                $(".menu-button-expand").removeClass("active");
            });
            
            
            
        })
    </script>
</header>