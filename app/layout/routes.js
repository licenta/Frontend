import "./sidebar.tag"
import "./header.tag"
import "./layout.tag"
import "./user-settings.tag"
import {getCookie} from "~/util/util"
import {load_session} from "~/common/authentification/authService"
import riot from "riot"
import {reduxStore} from "~/reduxStore"

const states = [{
    name: 'layout',
    route: '',
    template: 'layout',
    resolve: function(data,param,call){

        var response = getCookie("response")
        //if we are logged in the user can't see the login page
        if (response !== "") {
            load_session(response)
            call(null, null)
        }else{
            console.log("You are not logged in")
            window.location = "#/login"
        }
    },
    activate:()=>{
        var role = sessionStorage.getItem("role")
        if(role !== null){
            riot.mount("sidebar-role", role + "-sidebar")
           // riot.mount("profile-role", "profile-" + role )
        }else{
            riot.mount("sidebar-role","not-found-page")
           // riot.mount("profile-role", "not-found-page")
        }
    }
    //if you want the layout to automatically go to course page uncomment the line below
    //defaultChild: 'course',
}, {
    name: 'route_not_found',
    route: '/route_not_found',
    template: 'not-found-page',
    activate: function (context) {
        var tag = context.domApi
        tag.opts.path = context.parameters.path
    }
}]

export default states
