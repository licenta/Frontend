<sidebar>

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <nav id="spy">
            <!--this is where the common(all the users have it) nav go -->
            <ul class="sidebar-nav">
                <sidebar-role></sidebar-role>
            </ul>
        </nav>
    </div>
</sidebar>