<user-options>
    <ul class="nav navbar-nav navbar-right">
        <li>
            <div>
                <div id="user-data">
                    <img src="http://www.keenthemes.com/preview/conquer/assets/plugins/jcrop/demos/demo_files/image1.jpg"
                            alt="" class="img-circle">
                    <span>{this.opts.username}</span>
                </div>
                <span onclick="{this.opts.logout}" aria-label="Left Align">
                    Logout
                    <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                </span>
                <a href="#" aria-label="Left Align">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </a>
            </div>
        </li>
    </ul>    
</user-options>