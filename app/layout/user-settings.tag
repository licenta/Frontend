<user-settings>
    <div class="modal fade" id="{role}" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal" style="float:right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" style="float:left">Settings</h4>
                </div>
                <div class="modal-body row">
                    <form id="usrSet">
                         <div class="col-md-12 col-xs-12">
                            <label>User Name:</label>
                            <p><input type="text" name="name" id="name" value="{this.user.name}" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>E-mail:</label>
                            <p><input type="email" name="email" id="email" value="{this.user.email}" style="border-radius:4px;width:100%" readonly></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>First Name:</label>
                            <p><input type="text" name="firstname" id="firstname" value="{this.user.first_name}" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>Last Name:</label>
                            <p><input type="text" name="lastname" id="lastname" value="{this.user.last_name}" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>New Password:</label>
                            <p><input type="password" name="pass" id="pass" placeholder="Password" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>Confirm Password:</label>
                            <p><input type="password" name="pass_conf" id="confirm-pass" placeholder="Confirm Password" style="border-radius:4px;width:100%"></p>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <label>Image:</label>
                            <p><input type="file" name="image" id="image" placeholder="Image" style="border-radius:4px;width:100%"></p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger btnCancel"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        <button class="btn btn-primary btnYes" type="button"
                                onclick="{execute_action}" ><span class="glyphicon glyphicon-check"></span> Save Changes
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
    </div>
    <script>
        var vm = this;
         this.role = sessionStorage.getItem("role")
         this.user = JSON.parse(sessionStorage.getItem("userdata"))
         import {globals, $http} from "~/util/util"
         import {getCookie, setCookie} from "~/util/util"
         this.id = JSON.parse(sessionStorage.getItem("userdata")).id
         
          this.on("mount", function(){
             $("#usrSet").validate({
                 rules:{
                     name:{
                         required:true,
                         minlength:2
                     },
                     firstname:{
                         required:true,
                         minlength:2
                     },
                     lastname:{
                         required:true,
                         minlength:2
                     }
                 },
                 messages:{
                    name:"User name is required",
                    firstname:"First name is required and must have at least 2 characters",
                    lastname:"Last name is required and must have at least 2 characters",
                 },
                 submitHandler: function() {
                    if(vm.image.files[0]){
                          var data = {
                            "password": vm.pass.value,
                            "first_name": vm.firstname.value,
                            "last_name": vm.lastname.value,
                            "upload": vm.image.files[0],
                            "name": vm.name.value
                        }  
                    }else{
                         var data = {
                            "password": vm.pass.value,
                            "first_name": vm.firstname.value,
                            "last_name": vm.lastname.value,
                            "name": vm.name.value
                        }
                    }
                   
                    
                    $http.post(globals.BASE_API + "/user/update/" + vm.id + "?client_id="+ globals.client_id, data)
                        .then(function(response){
                         var res = JSON.parse(getCookie("response"));
                         var request_response = JSON.parse(response);
                         res.name = request_response.user.name;
                         res.first_name = request_response.user.first_name;
                         res.last_name = request_response.user.last_name;
                         setCookie("response",JSON.stringify(res));
                         sessionStorage.setItem("userdata", JSON.stringify(JSON.parse(response).user));
                         sessionStorage.setItem("name", JSON.parse(response).user.name);
                         
                        }).catch((error) => {
                            var message = JSON.parse(error.responseText).error.name;
                            console.log(error);
                        })
                    
                   $("#" + vm.role).modal('hide');
                 }
             });
         });
         
         this.execute_action = function(){
             $("#usrSet").submit();
         }
    </script>
</user-settings>