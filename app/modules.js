import {logout} from "./common/authentification/authService"

const module1 = {
    load_role: (handler) => {
        var role = sessionStorage.getItem("role")
        switch (role) {
            case "admin":
                require.ensure([], ()=> {
                    require("./core/admin/routes")
                    if (typeof handler == "function")
                        handler()
                }, "admin_module")
                break
            case "student":
                require.ensure([], ()=> {
                    require("./core/student/routes")
                    if (typeof handler == "function")
                        handler()
                }, "student_module")
                break
            case "teacher":
                 require.ensure([], ()=> {
                    require("./core/teacher/routes")
                    if (typeof handler == "function")
                        handler()
                }, "teacher_module")
                break
            default:
                console.log("User is not logged in,no role is set")
                logout()
        }
    }
}

export default module1