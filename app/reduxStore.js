import configureStore from "./redux_config/configureStore"
import ReducerRegistry from "./redux_config/ReducerRegistry"

function initialReducers(state = {}, action = {}) {
    switch (action.type) {
        default:
            return state
    }
}
var reducerRegistry = new ReducerRegistry(initialReducers)

var reduxStore = configureStore(reducerRegistry)

export {reduxStore,reducerRegistry}
