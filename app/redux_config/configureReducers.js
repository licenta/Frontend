import {combineReducers} from "redux"
// Import core third-party reducers here, e.g.:
//import {formReducer} from "redux-form"

function configureReducers(reducers) {
    return combineReducers(Object.assign({},...reducers)
        // Combine core third-party reducers here, e.g.:
        // form: formReducer,
        //formReducer
    )
}

export default configureReducers