import state_Router from "abstract-state-router"
import riotRenderer from "riot-state-renderer"
import domready from "domready"

import module from "./modules"

import states from "./common/routers"
import states_layout from "~/layout/routes"

import "./layout/not-found-page.tag"
import {load_session} from "./common/authentification/authService"
import {getCookie} from "./util/util"

const stateRouter = state_Router(riotRenderer({}), 'body')

stateRouter.on("routeNotFound",function(path,params){
    //the replace attribute will remove the invalid url from browser history
    //this is need for the back button to work properly
    stateRouter.go("route_not_found",{path:path},{replace:true})
})

function load_states(states){
    states.forEach((state)=> {
        stateRouter.addState(state)
    })
}

domready(function () {
    //load the corresponding states based on sessionStorage info
    load_states([...states_layout,...states])

    var response = getCookie("response")
    //if we are logged in the user can't see the login page
    if (response !== "") {
        load_session(response)
        //load the proper files
        module.load_role()
    }else{
        stateRouter.evaluateCurrentRoute("login")
    }
    if (!sessionStorage.getItem("role")) {
    } else {
    }
    // if (!sessionStorage.getItem("role")) {
    //     stateRouter.evaluateCurrentRoute("login")
    // } else {
    //     //load the proper files
    //     module.load_role()
    // }
})

export {stateRouter,load_states}