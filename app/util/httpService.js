var token = "";
function init_headers() {
    //init the token
    token = sessionStorage.getItem("token")
    http.default.settings.headers = {
        "authorization": "Bearer " + token
    }
}

function form_data(data) {
    //this will construct the form data
    var form = new FormData();
    for (var key in data) {
        form.append(key, data[key])
    }
    return form;
}

const http = {
    default: {
        settings: {
            "async": true, 
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data",
            //the authorization token will go in every token
            // only if the headers won't be overwritten
            "headers": {
                "authorization": "Bearer " + token
            }

        }
    },

    get(url, settings){
        init_headers()
        var promise = new Promise(function (resolve, reject) {
            var set = Object.assign({
                "url": url,
                "method": "GET",
                "success": function (response) {
                    resolve(response)
                },
                "error": function (response) {
                    reject(response)
                }
            }, http.default.settings, settings)
            //send the request
            $.ajax(set)
        })

        return promise
    },
    post_raw(url,data,settings){
        init_headers()
        var promise = new Promise(function (resolve, reject) {
            var set = Object.assign({
                "url": url,
                "method": "POST",
                "data": JSON.stringify(data),
                'processData': false,
                "success": function (response) {
                    resolve(response)
                },
                "error": function (response) {
                    reject(response)
                }
            }, http.default.settings, settings)
            //send the request
            $.ajax(set)
        })
        return promise
    },
    post(url, data, settings){
        init_headers()
        var promise = new Promise(function (resolve, reject) {
            var set = Object.assign({
                "url": url,
                "method": "POST",
                "data": form_data(data),
                "success": function (response) {
                    resolve(response)
                },
                "error": function (response) {
                    reject(response)
                }
            }, http.default.settings, settings)
            //send the request
            $.ajax(set)
        })

        return promise
    },
    put(url, data, settings){
        init_headers()
        var promise = new Promise(function (resolve, reject) {
            var set = {}
            console.log(data);
            Object.assign(set, http.default.settings, {
                "url": url,
                "method": "PUT",
                "data": $.param(data),
                "success": resolve,
                "error": reject,
                "contentType": 'application/x-www-form-urlencoded; charset=UTF-8'
            }, settings);
            //send the request
            $.ajax(set)
        //}
        })

        return promise
    },
    delete(url,settings){
        init_headers()
        var promise = new Promise(function (resolve, reject) {
            var set = Object.assign({
                "url": url,
                "method": "DELETE",
                "success": function (response) {
                    resolve(response)
                },
                "error": function (response) {
                    reject(response)
                }
            }, http.default.settings, settings)
            //send the request
            $.ajax(set)
        })

        return promise
    },
    patch(url, data, settings){
        init_headers()
        var promise = new Promise(function (resolve, reject) {
            var set = Object.assign({
                "url": url,
                "method": "PATCH",
                "data": form_data(data),
                "success": function (response) {
                    resolve(response)
                },
                "error": function (response) {
                    reject(response)
                }
            }, http.default.settings, settings)
            //send the request
            $.ajax(set)
        })
        return promise
    }
}

export default http;