import  $http from "./httpService"
import {BASE_API,BASE,client_id,client_secret,grant_type,BASE_PROXY} from "./globals"

const globals = {
    BASE_API:BASE_API,
    BASE:BASE,
    BASE_PROXY:BASE_PROXY,
    client_id:client_id,
    client_secret:client_secret,
    grant_type:grant_type
}

function setCookie(cname, cvalue, sec) {
    var d = new Date();
    //we add the seconds we want to the cookie expiration, the 1000 constant represents
    //the number of milliseconds in a second
    d.setTime(d.getTime() + sec * 1000);
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)===' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

export {globals, $http, getCookie, setCookie}