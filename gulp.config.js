//require('./custom-tools.js')();

module.exports = function () {
    return {
        "path": {
            "concat": {
                "style": "style.min.css",
                "libs": "libs.min.js"
            },
            "libs": {
                //in libs we put the javascript and the css that we want to use
                "src": [
                    "assets/libs/jquery-2.2.1.min.js",
                    "assets/libs/bootstrap.js",
                    "node_modules/bootstrap-select/js/bootstrap-select.js",
                    "bower_components/moment/moment.js",
                    "bower_components/fullcalendar/dist/fullcalendar.min.js",
                    "bower_components/jquery-validation/dist/jquery.validate.min.js",
                    "assets/libs/jquery-ui.js"
                ],
                "dest": "public/build"
            },
            "styles": {
                //poate ar trebui sa adaug in src si assets/**/*.css care sau fisierele
                //care nu sunt incluse in style.scss dar le folosesc
                "src": [
                    "assets/scss/style.scss",
                    //supravegheaza fisierele .css care nu sunt include in fisierul de deasupra
                    "assets/scss/*.scss"
                ],
                "watch": "assets/**/*",
                "dest": "public/build/"
            },
            "images": {
                "src": "assets/img/**",
                "dest": "public/build/img"
            }
            ,
            "fonts": {
                "src": "assets/fonts/*",
                "dest": "public/build/fonts"
            }
        },
        "livereload": {
            "proxyServer": 'http://localhost:3999/',
            "path": "public/",
            "port": 3601,
            "flags": "--quiet"
        }
    }
};
