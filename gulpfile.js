var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    prefix = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    config = require('./gulp.config.js')(),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    watch = require('gulp-watch');

//error log function:
function errorLog(error) {
    console.error(error.message);
}


//Scripts task:
//Uglifies
gulp.task('compress-js', function () {

    //3rd libraries(like jquery):
    gulp.src(config.path.libs.src)
        .pipe(concat(config.path.concat.libs))
        .pipe(uglify())
        .on('error', errorLog)
        .pipe(gulp.dest(config.path.libs.dest))

});
//
//Style task:
//Uglifies
//
gulp.task('styles', function () {
    gulp.src(config.path.styles.src)
        .pipe(concat(config.path.concat.style))
        .pipe(sass({
            //to have my sass css compresed:
            outputStyle: 'compressed'
            //outputStyle: 'extended'
        }))
        .on('error', errorLog)
        .pipe(prefix({
            browsers: ['last 2 versions', 'safari 5'],
            cascade: false
        }))
        .pipe(gulp.dest(config.path.styles.dest))
});

//Images task:
//Compress the images
gulp.task('images', function () {
    gulp.src(config.path.images.src)
        .pipe(imagemin())
        .pipe(gulp.dest(config.path.images.dest))
});

// Task fonts
gulp.task('fonts', function () {
    return gulp.src(config.path.fonts.src)
        .pipe(gulp.dest(config.path.fonts.dest));
});

//Watch task:
//Watches JS
gulp.task('watch', function () {

    watch(config.path.styles.watch, function () {
        gulp.run(['styles']);
    });

    watch(config.path.images.src, function () {
        gulp.run(['images']);
    });

    watch(config.path.fonts.src, function () {
        gulp.run(['fonts']);
    });

});

gulp.task('browser-sync', function () {
    browserSync({
        proxy: config.livereload.proxyServer,
        port: config.livereload.port,
        files: config.livereload.path,
        ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: true
        },
        watch: false,
        logFileChanges: false,
        open: false
    });
});

gulp.task('proxy-back', function () {
    browserSync({
        proxy:'http://licenta-back.dev/',
        port: config.livereload.port,
        files: config.livereload.path,
        ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: true
        },
        watch: false,
        logFileChanges: false,
        open: false
    });
});

//Build task
gulp.task('build', function () {
    //build all source code
    gulp.start('compress-js','styles', 'images', 'fonts');
});

////Default Task
gulp.task('default', ['compress-js','fonts', 'styles', 'browser-sync', 'watch']);
