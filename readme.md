# Frontend for licenta

RiotJs seed project with webpack on any host machine including [Windows](#headin) :))

## config

To configure you environmental variable config webpack.config.js gulp.config.js


## Tools needed:

1. [nodejs](https://nodejs.org/en/) If you are on a windows host you also need:
 

## Install dev tools

+ open a terminal in the root folder of the project and type:

```
$ npm install
```
- this will install dependencies from package.json

### Configure the .example files

```
 cp app\common\util\globals.example.js app\common\util\globals.js
```
- this is on ubuntu

### Run it

```
npm run dev
```

- 1.1 For development if you have a server and you want to proxy it

or
```
npm run dev_2
```
- 1.2 For development if you don't have a server the defalut port path will be:
- http://localhost:8080

```
npm run prod
```

- 2.For production

- compile the scripts(riotjs tags,javascript,scss) 
- concat, minifies and put them in the build folder
- compress the images

### Webpack && gulp commands ###

- the instructions are in the gulpfile and in the webpack.config

# Proxy Server

Proxy server in express, for 
* **POST** /oauth/access_token

### Installation

Copy .env.example to .env and change .env according to your needs.

```sh
$ npm start
```