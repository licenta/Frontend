var express = require('express')
var request = require('request')
var path = require('path')
var router = express.Router()

router.get('/', function (req, res) {
    res.header('csrf-token', req.csrfToken())
    res.sendFile(path.join(__dirname + '/../public/index.html'));
});

module.exports = router;
