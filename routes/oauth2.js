var express = require('express')
var request = require('request')
var router = express.Router()

router.get('/authorize', function (req, res) {
    req.query.master_client_id = process.env.CLIENT_ID
    req.query.master_client_secret = process.env.CLIENT_SECRET
    request.get(process.env.TARGET_URL + '/oauth/authorize?with=Role', {
        qs: req.query,
        headers: {
            Authorization: req.headers['authorization']
        }
    }).pipe(res);
});

router.post('/authorize', function (req, res) {
    console.log("dda" + process.env.CLIENT_SECRET)
    request.post(process.env.TARGET_URL + '/oauth/authorize', {
        qs: req.body,
        form: {
            master_client_id: process.env.CLIENT_ID,
            master_client_secret: process.env.CLIENT_SECRET
        },
        headers: {
            Authorization: req.headers['authorization']
        }
    }).pipe(res)
});

router.post('/access_token/password', function (req, res) {
    request.post(process.env.TARGET_URL + '/oauth/access_token', {
        form: {
            grant_type: 'password',
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET,
            scope: 'advanced_user_read,admin_write,basic_user_read,oauth_manager,professor_write',
            username: req.body.username,
            password: req.body.password
        }
    }).pipe(res)
});

module.exports = router;
