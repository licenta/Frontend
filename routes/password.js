var express = require('express')
var request = require('request')
var router = express.Router()

router.post('/email', function (req, res) {
    req.body.master_client_id = process.env.CLIENT_ID
    req.body.master_client_secret = process.env.CLIENT_SECRET
    request.post(process.env.TARGET_URL + '/password/email', {
        form: req.body
    }).pipe(res)
});

router.post('/reset', function (req, res) {
    req.body.master_client_id = process.env.CLIENT_ID
    req.body.master_client_secret = process.env.CLIENT_SECRET
    request.post(process.env.TARGET_URL + '/password/reset', {
        form: req.body
    }).pipe(res)
});

module.exports = router;
