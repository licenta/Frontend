var debug = process.env.NODE_ENV != "production ";

var webpack = require('webpack'),
    CleanWebpackPlugin = require('clean-webpack-plugin');

var prod_plugins = [
    new CleanWebpackPlugin(['dist', 'build'], {
        root: __dirname + '/public',
        verbose: true,
        dry: false
    })];

//here we configure the chunks
var commonsPlugin = [
    new webpack.optimize.CommonsChunkPlugin({
        // process all children of the main chunk
        // if omitted it would process all chunks
        name: "common",
        // create a additional async chunk for the common modules
        // which is loaded in parallel to the requested chunks
        async: true
    })
];
commonsPlugin.forEach(function (entry) {
    prod_plugins.push(entry);
})

prod_plugins.push(new webpack.optimize.DedupePlugin());
prod_plugins.push(new webpack.optimize.OccurenceOrderPlugin());
prod_plugins.push(new webpack.optimize.UglifyJsPlugin({mangle: false, sourcemap: false}));

module.exports = {
    entry: {
        app: './app/main.js'
    },
    output: {
        path: "public/build",
        publicPath: "build/",
        filename: '[name].js',
        sourceMapFilename: "[name].map"

    },
    devtool: debug ? '#source-map' : '',
    module: {
        preLoaders: [
            {
                test: /\.js?$/,
                loaders: ['jshint'],
                // define an include so we check just the files we need
                include: /app/,
                exclude: [/node_modules/, /assets/, /public/]
            }
        ],
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        },
            {
                test: /\.tag?$/,
                loader: 'tag-loader',
                exclude: /(node_modules|bower_components)/,
                query: {
                    type: 'babel'
                }
            }
        ]
    },
    devServer: {
        contentBase: './public'
    },
    plugins: debug ? commonsPlugin : prod_plugins
};
